<html>
<head>
<title>Inserimento Domande</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
</head>
<body>
<script src="assets/scripts/caricaCategorie.js"></script>
	Selezionare il tipo di domanda:
	<input type="radio" name="tipo-domanda" value="aperta">Aperta
	<input type="radio" name="tipo-domanda" value="chiusa">Chiusa
	<div id="inserimento-domande">
	<form action="webapi/domande" method="POST" id="form-domanda" enctype="multipart/form-data" accept-charset=utf-8>
		<script type="text/javascript">
		$(".categorie").hide();
			$('input[name="tipo-domanda"]').on('change', function() {
				$(".categorie").show();		
				var scelta = $('input[name="tipo-domanda"]:checked').val();
				var form = $("#form-domanda");
					if (scelta == "aperta") {
						$("#form-domanda").empty();
						form.append("<legend>Informazioni domanda:</legend>");
						form.append("Testo*: <textarea rows=\"4\" cols=\"50\" name=\"testo\" placeholder=\"Inserire qui il testo della domanda.\"required></textarea><br/>")
						form.append("Risposta: <input type=\"text\" name=\"risposta\"> <br />");
						form.append("Immagine: <input name=\"immagine\" type=\"file\" size=\"20\" id=\"immagine\" accept=\"image/*\"><br />");
						form.append("Spazi bianchi dopo il testo della domanda: <input type=\"number\" min=0 name=\"spazi-bianchi\" value=\"5\">");
						form.append("<br/> Punteggio Base: <input type=\"number\" name=\"punteggio-base\" min=\"1\" max=\"5\" value =\"1\"> <br />");
						form.append("<div id=\"categorie\">Categorie*: </div>");
						form.append("<input type = 'submit' id='submit' value ='Submit'>");
						form.append("<input type = 'reset' value ='Reset'><br />");
						form.append("<div id=\"no-category\">Categoria non presente? Clicca <a href=\"categorie-form.jsp\">qui</a> per aggiungerne una nuova.</div>");
						caricaCategorie();
						prepareAjaxSubmit();
								} else {
									$("#form-domanda").empty();
									form.append("<legend>Informazioni domanda:</legend>");
									form.append("Testo*: <textarea rows=\"4\" cols=\"50\" name=\"testo\" placeholder=\"Inserire qui il testo della domanda.\"required></textarea><br/>")
									form.append("<br/> Punteggio Base: <input type=\"number\" name=\"punteggio-base\" min=\"1\" max=\"5\" value =\"1\"> <br />");
									form.append("<div class=\"opzioni\" id =\"option-type\">Opzioni*: <input type=\"text\" id=\"option0\" onchange=\"myFunction1()\"; name =\"opzioni-risposta\" required><input type=\"text\" id=\"option1\" onchange=\"myFunction2()\"; name =\"opzioni-risposta\" required><input type=\"text\" id=\"option2\" onchange=\"myFunction3()\"; name =\"opzioni-risposta\" required><input type=\"text\" id=\"option3\" onchange=\"myFunction4()\"; name =\"opzioni-risposta\" required></div>");
									form.append("Risposta corretta*: <select name=\"risposta\" id=\"risposta-corretta\" required></select> <br />");
									form.append("<div id=\"categorie\">Categorie*: </div>");
									form.append("<input type = 'submit' id='submit' value ='Submit'>");
									form.append("<input type = 'reset' value ='Reset'><br />");
									form.append("<div id=\"no-category\">Categoria non presente? Clicca <a href=\"categorie-form.jsp\">qui</a> per aggiungerne una nuova.</div>");
									caricaCategorie();
									prepareAjaxSubmit();
								}
					var source = document.location.href;
					$("#no-category a").attr("href", "categorie-form.jsp?from="+source);
							});
		</script>
		<script>
			function myFunction1(){
				var o0 = document.getElementById("option0").value;
				$("#risposta-corretta").append("<option value=\""+o0+"\">"+o0+"</option>");
			}
			function myFunction2(){
				var o1 = document.getElementById("option1").value;
				$("#risposta-corretta").append("<option value=\""+o1+"\">"+o1+"</option>");
			}
			function myFunction3(){
				var o2 = document.getElementById("option2").value;
				$("#risposta-corretta").append("<option value=\""+o2+"\">"+o2+"</option>");
			}
			function myFunction4(){
				var o3 = document.getElementById("option3").value;
				$("#risposta-corretta").append("<option value=\""+o3+"\">"+o3+"</option>");
			}
		</script>
	</form>
	</div>
	<script type="text/javascript">
	function prepareAjaxSubmit() {
          $("#submit").click(function(){
             var form = $(this).parents("form")[0];
             var formData = new FormData(form);
             $.ajax({
            	 url: $(form).attr("action"), 
                 type: 'POST',
                 xhr: function() {  // Custom XMLHttpRequest
                     var myXhr = $.ajaxSettings.xhr();
                     return myXhr;
                 },
				 data : formData,
				 cache: false,
			     contentType: false,
			     processData: false,
            	 success: function(event){
            	 	console.log(event);
            	 	window.location.replace("domande.jsp");
             	 }
             }).error(function(error){
             	alert("Codice errore: " + error.responseJSON.codice + "\nMessaggio: " +error.responseJSON.messaggio+ "\nDocumentazione: " +error.responseJSON.documentazione);
             });
             return false;
        });
	}
          
</script>
</body>
</html>
