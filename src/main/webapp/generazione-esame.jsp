<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Generazione esame</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
<jsp:include page="/includes/head.jsp"></jsp:include>
</head>
<body>
	<h1>Generazione esame</h1>
	<jsp:include page="/includes/menu.jsp"></jsp:include>
	<div class="error-message"></div>
	<form action="webapi/esami" method="POST" id="form-esame">
	<div id="data-e-appello">
		<div id="data" class="date-appello">Data esame*: <input type="date" id="data_esame" name = "data-esame"  required placeholder="Inserire la data nel formato AAAA-MM-DD" size=40></div>
		<div id="appello" class="date-appello">Appello numero*: <input type="number" id="numero-appello" name = "appello" max=7 min=1 required></div>
	</div><br />
	<div id="loghi">Aggiungere logo bicocca?<input type="radio" name="logoBicocca" value=1> Sì
					<input type="radio" name="logoBicocca" value=0 checked> No <br/>
					Aggiungere logo DISCo?<input type="radio" name="logoDisco" value=1> Sì
					<input type="radio" name="logoDisco" value=0 checked> No</div>
		<div id="intestazione">
			<p id="intext-uni"><b>Università: </b> </p>
			<p id="intext-cdl"><b>Corso di laurea: </b></p>
			<p id="intext-aa"><b>Anno accademico: </b></p>
			<p id="intext-corso"><b>Nome corso: </b></p>
			<p id="intext-regole"><b>Regole: </b></p>
			<p id="edit-info-redirect">Vuoi cambiare intestazione? <a href="">Clicca qui</a></p>
		</div>
		<div id="numeriDomande"></div>
		<div id="filter-question">
		 <p id="domande-per-categoria-paragraph">Visualizza le domande della categoria: <select id="domande-per-categoria"></select><button id="filter-by-category">Filtra</button></p>
		</div>
		<table id="tabella" class="tabella"></table>
		<div id="page-number"></div>
		<div id="domande-selezionate" style="display:none;"><h2>Domande selezionate</h2><table id="domande-aggiunte" class="tabella"></table></div>
		<script>
		var source = document.location.href;
		$("#edit-info-redirect a").attr("href", "info-edit.jsp?from="+source);
			$("#tabella")
					.append(
							"<th>#</th><th>Domanda</th><th>Tipo</th><th>Punti</th><th id=\"appelloOrder\">Appelli</th><th id=\"counterOrder\">Contatore</th>");
		</script>
		<script>
			$("#domande-aggiunte")
					.append(
							"<th>#</th><th>Domanda</th><th>Tipo</th><th>Punti</th><th id=\"appelloOrder\">Appelli</th><th id=\"counterOrder\">Contatore</th>");
		</script>
		<script>
		var domandeTotali;
		$.getJSON('./webapi/domande', function(data){
			domandeTotali = data.length;
			});
		pageSize=10;
		quest = [];
		domAggSize=0;
		$("#domande-aggiunte").hide();
		$.getJSON('./webapi/domande?order=appello&start=1&size='+pageSize)
			.done(function(data) {
				numbers = [];
				pages = Math.ceil(domandeTotali/pageSize);
				if (pages == 1){
				}else{
					for (var i = 0; i < pages; i++){
						pN = i+1;
						$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePage("+pN+");\"><u>"+pN+"</u></div>");
					}
				}
								for (var i = 0; i < data.length; i++) {
									var item = data[i];
									var tipo;
									numbers.push(item.numero);
									if (item.aperta == true){
										tipo = "Aperta";
									}
									else{
										tipo = "Crocette";
									}
									if($.inArray(item.numero, quest) != -1){
									}else{
										$("#tabella")
										.append(
												"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
														+ item.testoDomanda
														+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
														+ item.appelli
														+ "</td><td>"
														+ item.contatore
														+ "</td></tr>");}
								}})
								.error(function(error){
							console.log(error);
				    		$("#tabella").hide();
				    		$("#form-esame").hide();
				    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
						});
		</script>
		<!-- 
		<script>
			$(document).ready(function(){
				$("#counterOrder").click(function(){
					$('tbody').empty();
					$.getJSON('./webapi/domande?order=count')
					.done(function(data) {
										for (var i = 0; i < data.length; i++) {
											var item = data[i];
											var tipo;
											if (item.aperta == true)
												tipo = "Aperta";
											else
												tipo = "Crocette";
											if($.inArray(item.numero, quest) != -1){
											}else{

											$("#tabella")
											.append(
													"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
															+ item.testoDomanda
															+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
															+ item.appelli
															+ "</td><td>"
															+ item.contatore
															+ "</td></tr>");}
										}})
										.error(function(error){
									console.log(error);
						    		$("#tabella").hide();
						    		$("#form-esame").hide();
						    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
						    		var delay = 3000; //Your delay in milliseconds
						    		setTimeout(function(){ window.location.replace("domande.jsp"); }, delay);
								});
				});
			});
		</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$("#appelloOrder").click(function(){
				$('tbody').empty();
				$.getJSON('./webapi/domande?order=appello')
				.done(function(data) {
									for (var i = 0; i < data.length; i++) {
										var item = data[i];
										var tipo;
										if (item.aperta == true)
											tipo = "Aperta";
										else
											tipo = "Crocette";
										if($.inArray(item.numero, quest) != -1){
										}else{
										$("#tabella")
										.append(
												"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
														+ item.testoDomanda
														+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
														+ item.appelli
														+ "</td><td>"
														+ item.contatore
														+ "</td></tr>");}
									}})
									.error(function(error){
								console.log(error);
					    		$("#tabella").hide();
					    		$("#form-esame").hide();
					    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
					    		var delay = 3000; //Your delay in milliseconds
					    		setTimeout(function(){ window.location.replace("domande.jsp"); }, delay);
							});
			});
		});
		</script>
		-->
		<input type="submit"  id= "submit" value="Popola esame"> <input type="reset" id="reset"
			value="Annulla">
 
	</form>
	<script>
          $("#submit").click(function(){
             var form = $(this).parents("form");
             var data = $('form').serialize();
             $.post($(form).attr("action"), data)
             .done(function(event){
            	 window.location.replace("esami.jsp");
             })
            .error(function(error){
            	alert("Codice errore: " + error.responseJSON.codice + "\nMessaggio: " +error.responseJSON.messaggio+ "\nDocumentazione: " +error.responseJSON.documentazione);
});
             return false;
           });
</script>
<script>
$("#reset").click(function(){
	$("#domande-aggiunte tbody").empty();
	$("#domande-selezionate").hide();
	quest = [];
});
</script>
	<script>
	$(document).ready(function(){
		var dom = [];
		$.get("webapi/esami")
		.done(function(data){
			var max = 1;
			for (var i = 0; i < data.length; i++){
				var item = data[i];
				if (item.appelloNumero >= max){
					max = item.appelloNumero +1;
				}
			}
			$("#numero-appello").val(max);
		})
		   .error(function(error){ 
			   $("#numero-appello").val(1);
			   });
		$.getJSON('./webapi/categorie')
		.done(function(data){
			var categ = data;
			if (categ.length == 1){
				$("#domande-per-categoria-paragraph").hide();
			}else {
			for (var i = 0; i < categ.length; i++){
				$("#domande-per-categoria").append("<option value=\""+categ[i].nome+"\">"+categ[i].nome+"</option>");
			}
				//$("#domande-per-categoria").append("<option value=\"\" selected>Tutte</option>");
			}
		});
	});
			
	</script>
	<script>
	$('#filter-by-category').on("click", function(e){
	    e.preventDefault();
	    var totDomande = 0;
	    $("#tabella tbody").empty();
	    var selected = $("#domande-per-categoria option:selected").val();
	    if (selected == ""){
	    	//window.location.replace("generazione-esame.jsp");
	    }
	    $.getJSON('./webapi/domande?categoria='+selected, function(data){
	    	totDomande = data.length;
	    });
	    $.getJSON('./webapi/domande?categoria='+selected+'&start=1&size='+pageSize)
	    .done(function(data) {
	    	$("#page-number").empty();
	    	pages = Math.ceil(totDomande/pageSize);
	    	if (pages == 1){
	    	}else{
	    		for (var i = 0; i < pages; i++){
					pN = i+1;
					$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePageByCategory("+pN+");\"><u>"+pN+"</u></div>");
				}
	    	}
	    	for (var i = 0; i < data.length; i++) {
				var item = data[i];
				var tipo;
				if (item.aperta == true)
					tipo = "Aperta";
				else
					tipo = "Crocette";
				if($.inArray(item.numero, quest) != -1){
				}else{
				$("#tabella")
				.append(
						"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
								+ item.testoDomanda
								+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
								+ item.appelli
								+ "</td><td>"
								+ item.contatore
								+ "</td></tr>");}
			}})
		.error(function(error){
	console.log(error);
	$("#tabella").hide();
	$("#form-esame").hide();
	$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	var delay = 3000; //Your delay in milliseconds
	setTimeout(function(){ window.location.replace("generazione-esame.jsp"); }, delay);
});
	});
	</script>
	<script>
		var today = moment().format('YYYY-MM-DD');
		$('#data_esame').val(today);
	</script>
	<script>
		function addQ(numero){
			numbers = jQuery.grep(numbers, function(value) {
				  return value != numero;
			});
			quest.push(numero);
			console.log(quest);
			if (quest.length > 0)
				$("#domande-selezionate").show();
			$("#numeriDomande").append("<input type=\"checkbox\" name=\"numeroDomanda\" style=\"display:none\" id=\"checkNumero"+numero+"\" value="+numero+" checked>");
			domAggSize++;
			$("#domande-aggiunte").show();
			$.getJSON('./webapi/domande/'+numero)
			.done(function(data){
				var item = data;
				var tipo;
				if (item.aperta == true){
					tipo = "Aperta";
				}
				else{
					tipo = "Crocette";
				} 
				$("#question"+numero).remove();
				$("#domande-aggiunte").append("<tr id=\"questionAdd"+item.numero+"\"><td><img class=\"removeQuestion\" src=\"assets/images/webapp/remove.png\" onclick=\"removeQ("+item.numero+")\"></td><td>"
						+ item.testoDomanda
						+ "</td><td>"+tipo+"</td><td><input type=\"number\" id=\"overridePunt\" name=\"puntiCambiati\" min=1 value=\""+item.punteggioBase+"\"></td><td>"
						+ item.appelli
						+ "</td><td>"
						+ item.contatore
						+ "</td></tr>");
			});
		}
		function removeQ(numero){
			domAggSize--;
			numbers.push(numero);
			quest = jQuery.grep(quest, function(value) {
				  return value != numero;
			});
			console.log(quest);
			if (quest.length == 0)
				$("#domande-selezionate").hide();
			if(domAggSize == 0)
				$("#domande-aggiunte").hide();
			$("#checkNumero"+numero).prop('checked', false);
			$.getJSON('./webapi/domande/'+numero)
			.done(function(data){
				var item = data;
				var tipo;
				if (item.aperta == true){
					tipo = "Aperta";
				}
				else{
					tipo = "Crocette";
				} 
				$("#questionAdd"+numero).remove();
				$("#tabella").append("<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
						+ item.testoDomanda
						+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
						+ item.appelli
						+ "</td><td>"
						+ item.contatore
						+ "</td></tr>");
			});
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.getJSON('./webapi/info')
			.done(function(data){
				var item = data[0];
				$("#intext-uni").append(item.uni);
				$("#intext-cdl").append(item.cDL);
				$("#intext-aa").append(item.annoAccademico);
				$("#intext-corso").append(item.nomeCorso);
				$("#intext-regole").append(item.regoleEsame);
			})
			.error(function(error){
				$("#edit-info-redirect a").attr("href", "info.jsp?from="+source);
			});
		});
	</script>
	<script>
	function changePage(lastPage){
		var start;
		if(lastPage != 1){
			lastPage--;
			start = lastPage*pageSize;
		}
		else{
			start = 1;
		}
		$('#tabella tbody').empty();
		$.getJSON('./webapi/domande?order=appello&start='+start+'&size='+pageSize)
		.done(function(data){
			for (var i = 0; i < data.length; i++) {
				var item = data[i];
				var tipo;
				if (item.aperta == true){
					tipo = "Aperta";
				}
				else{
					tipo = "Crocette";
				}
				if($.inArray(item.numero, quest) != -1){
				}else{
					$("#tabella")
					.append(
							"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
									+ item.testoDomanda
									+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
									+ item.appelli
									+ "</td><td>"
									+ item.contatore
									+ "</td></tr>");
				}
					
			}});
		}
	</script>
	<script>
		function changePageByCategory(lastPage){
			var start;
			var categoria = $("#domande-per-categoria").val();
			if(lastPage != 1){
				lastPage--;
				start = lastPage*pageSize;
			}
			else{
				start = 1;
			}
			$('#tabella tbody').empty();
			$.getJSON('./webapi/domande?categoria='+categoria+'&start='+start+'&size='+pageSize)
			.done(function(data){
				for (var i = 0; i < data.length; i++) {
					var item = data[i];
					var tipo;
					if (item.aperta == true){
						tipo = "Aperta";
					}
					else{
						tipo = "Crocette";
					}
					if($.inArray(item.numero, quest) != -1){
					}else{
	                $("#tabella").append(
	                		"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
							+ item.testoDomanda
							+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
							+ item.appelli
							+ "</td><td>"
							+ item.contatore
							+ "</td></tr>");
					}
	    		}}); 
		}
	</script>
</body>
</html>