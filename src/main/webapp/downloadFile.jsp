<html>
<head>
<title>Download files</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
</head>
<body>
<script>
var esame = "${param.esame}";
</script>
<div class="error-message"></div>
<ul id="lista-files">
<li id="pdf"><a href="">PDF</a></li>
<li id="pdf-sol"><a href="">PDF soluzioni</a></li>
<li id="fileEdit"><a href="">Formato editabile</a></li>
</ul>
</body>
<script>
	$.getJSON('./webapi/esami/'+esame, function(data){
		item = data;
		$("#pdf a").attr("href", "files/"+item.fileName + ".pdf");
		$("#pdf-sol a").attr("href", "files/"+item.fileName + "_SOL.pdf");
		$("#fileEdit a").attr("href", "files/"+item.fileName + ".odt");
	})
</script>
</html>
