<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Esame</title>
</head>
<body>
<jsp:include page="/includes/menu.jsp"></jsp:include>
<p id ="intestazione"></p>
<table id="esame" class="tabella"></table>
<form id="modifica-esame">
</form>
<div class="error-message"></div><br />
<div id="page-number"></div>
	<div id="anteprima">
		<div id="intestazione-esame"></div>
		<div id="elenco-domande">
			<ol id="ol-domande" type="1"></ol>
	</div>
</div>
	<script type="text/javascript">
	domandeTotali=0;
    $.getJSON('./webapi/domande', function(data){
    	domandeTotali = data.length;
    });
var idEsame = "${param.id}";
$.getJSON('./webapi/esami/' + idEsame)
.done(function (data){
	console.log(data.puntiDomande.entry);
	$("#domande-presenti").hide();
	$("#intestazione").append("Esame "+idEsame + " del " + data.data);
	var domande = data.domande;
	var congelato = data.congelato;
	var creato = data.creato;
	if (congelato == true){
		if (creato == true){
			var presenti;
			if(data.studentiPresenti == 0){
				presenti = "<button id=\"presenti\" onclick=\"inserisciPresenti("+idEsame+");\">Inserisci studenti presenti</button>";
			}
			else{
				presenti = "<div>"+data.studentiPresenti+"</div><button id=\"presenti\" onclick=\"aggiornaPresenti("+idEsame+", "+data.studentiPresenti+");\">Aggiorna studenti presenti</button>";
			}
			$("#esame").append("<th>Data</th><th>Numero domande</th><th>Appello</th><th>Studenti presenti</th><th>Files</th><th>Delete</th>");
			$("#esame").append("<tr><td>"+data.data+"</td><td>"+domande.length+"</td><td>"+data.appelloNumero+"</td><td id=\"stud-pres\">"+presenti+"</td><td><a href=\"downloadFile.jsp?esame="+idEsame+"\">Vai ai files</a></td><td><button onclick=\"confirmDeteleExam("+idEsame+")\">DELETE</button></td></tr>");
			for(var i = 0; i < domande.length; i++){
				var question = domande[i];
				$("#domande-list").append("<li>"+question.testoDomanda+"</li>");
			}
			$("#anteprima").empty();
			$("#domande-presenti").hide();
			$("#domande-selezionate").hide();
		}else{
			$("#esame").append("<th>Data</th><th>Numero domande</th><th>Appello</th><th>Sblocca</th><th>Stampa</th><th>Delete</th>");
			$("#esame").append("<tr><td>"+data.data+"</td><td>"+domande.length+"</td><td>"+data.appelloNumero+"</td><td><button onclick=\"sbloccaEsame("+idEsame+");\">Sblocca</button></td><td><button onclick=\"creaFiles("+idEsame+");\">Crea Files</button></td><td><button onclick=\"confirmDeteleExam("+idEsame+")\">DELETE</button></td></tr>");
			for(var i = 0; i < domande.length; i++){
				var question = domande[i];
				$("#domande-list").append("<li>"+question.testoDomanda+"</li>");
			}
		}
		
	}
	else{
		$("#esame").append("<th>Data</th><th>Numero domande</th><th>Appello</th><th>Blocca</th><th>Edit</th><th>Delete</th>");
		$("#esame").append("<tr><td>"+data.data+"</td><td>"+domande.length+"</td><td>"+data.appelloNumero+"</td><td><button onclick=\"congela("+idEsame+");\">Congela</button></td><td><button onclick=\"editFormExam("+idEsame+")\">EDIT</button></td><td><button onclick=\"confirmDeteleExam("+idEsame+")\">DELETE</button></td></tr>");
		for(var i = 0; i < domande.length; i++){
			var question = domande[i];
			$("#domande-list").append("<li>"+question.testoDomanda+"</li>");
		}
	}
	
})
.error(function(error){
		$("#tabella").hide();
		$("#anteprima").empty();
		$("#anteprima").hide();
   		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
   		var delay = 3000; //Your delay in milliseconds
		setTimeout(function(){ window.location.replace("esami.jsp"); }, delay);
});
</script>
<script>
	$(document).ready(function(){
		$.getJSON('./webapi/info')
		.done(function(data){
			item = data[0];
			$("#intestazione-esame").append("<h1>Anteprima esame</h1>");
			$("#intestazione-esame").append("<p>Università: "+item.uni + "</p>");
			$("#intestazione-esame").append("<p>Corso di laurea: "+item.cDL + "</p>");
			$("#intestazione-esame").append("<p>Nome corso: "+item.nomeCorso + "</p>");
			$("#intestazione-esame").append("<p>Anno accademico: "+item.annoAccademico + "</p>");
			$("#intestazione-esame").append("<p>Regole esame: "+item.regoleEsame + "</p>");
			$.getJSON('./webapi/esami/' + idEsame, function(data){
				var domandeEsame = data.domande;
				$("<h3 id=\"elencoTitle\">Elenco domande</h3>").insertBefore("#ol-domande");
				for (var i = 0; i < domandeEsame.length; i++){
					var domanda = domandeEsame[i];
					var numero = i+1;
					if (domanda.aperta == true){
						if(domanda.immagineDomanda == ""){
							$("#ol-domande").append("<li>"+numero+ ". "+domanda.testoDomanda+"</li>");
						}else{
							$("#ol-domande").append("<li>"+numero+ ". "+domanda.testoDomanda+"</li>");
							$("#ol-domande").append("<img class=\"exam-preview\"src=\"assets/images/domande/"+domanda.immagineDomanda+"\">");
						}
					}else{
						$("#ol-domande").append("<li>"+numero+ ". "+domanda.testoDomanda+"</li>");
						$("#ol-domande").append("Risposte: " + domanda.opzioniRisposta);
					}
					
				}
			});
			
		});
	});
</script>
<script>
	function inserisciPresenti(idEsame){
		$("#stud-pres").empty();
		$("#stud-pres").append("<input type=\"number\" min=0 max=500 value=0 id=\"studenti-presenti\">");
		$("#stud-pres").append("<input type=\"button\" value=\"Salva valore\" id=\"save-value\" min=0 max=500 onclick=\"updatePresenti("+idEsame+")\">");
	}
	
	function aggiornaPresenti(idEsame, presenti){
		$("#stud-pres").empty();
		$("#stud-pres").append("<input type=\"number\" min=0 max=500 value="+presenti+" id=\"studenti-presenti\">");
		$("#stud-pres").append("<input type=\"button\" value=\"Salva valore\" id=\"save-value\" min=0 max=500 onclick=\"updatePresenti("+idEsame+")\">");
	}
</script>
<script src="assets/scripts/scongelaEsame.js"></script>
<script src="assets/scripts/congelaEsame.js"></script>
<script src="assets/scripts/creaFiles.js"></script>
<script src="assets/scripts/editExam.js"></script>
<script src="assets/scripts/deleteExam.js"></script>
</body>
</html>