<html>
<head>
<title>Esami</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
</head>
<body>
    <p id="esami-per-anno-paragraph">Seleziona gli esami per anno: <select id="esami-per-anno"></select><button id="filter">Filtra</button></p>
    <table id="tabella" class="tabella"></table>
    <div id="page"></div>
	<form id="modifica-esame"></form>
	<div class="error-message"></div>
	<div id="page-number"></div>
	<script>
	$("#exams-back").hide();
		$("#tabella")
				.append(
						"<th>Id</th><th>Data</th><th>Appello</th><th>Stato</th><th>Risposte</th><th>Edit</th><th>Delete</th>");
	</script>
    <script type="text/javascript">
    domandeTotali=0;
    $.getJSON('./webapi/domande', function(data){
    	domandeTotali = data.length;
    });
    var dim = 4;
    $.getJSON('./webapi/esami', function(data){
    	var nEsami = data.length;
    	var pages = Math.ceil(nEsami/dim);
    	for(var i = 0; i < pages; i++){
    		var pageNumber = i+1;
    		//$("#page").append("<div id=\"pageNumber\""+pageNumber+" onclick=\"changePage("+dim+");\">"+pageNumber+"</div>");
    	}
    });
    $.getJSON('./webapi/esami?start=1&size=1000')
    .done(function(data) {
    	var size = data.length;
    	var page;
    	var button;
    	for (var i = 0; i < data.length; i++) {
    		var stato;
    		var risposte;
    		var item = data[i];
    		if (item.congelato == true){
    			if(item.creato == true){
    				risposte = "<a href=\"risposte-form.jsp?esame="+item.id+"\"><button>Inserisci risposte</button></a><a href=\"risposte.jsp?esame="+item.id+"\"><button>Vai alle risposte</button></a>";
    				stato = "Pdf gi� creato";
    			}
    			else {
    				risposte = "";
    				stato = "Bloccato - Pdf da creare";
    			}
    			button = " <button type=\"button\" class=\"disabled\" onclick=\"editFormExam("+item.id+")\" disabled>EDIT</button>";
    		}
    		else{
    			stato = "Editabile";
    			risposte = "";
    			button = " <button type=\"button\" class=\"enabled\" onclick=\"editFormExam("+item.id+")\">EDIT</button>";
    		}
           		$("#tabella").append(
    					"<tr><td><a href=\"esame.jsp?id="+item.id+"\">" + item.id + "</a></td><td>"
    							+ item.data + "</td><td>"
    							+ item.appelloNumero + "</td><td>"+stato+"</td><td>"+risposte+"</td><td>"+button+"</td><td><button onclick=\"confirmDeteleExam("
    							+ item.id
    							+ ")\">DELETE</button></td></tr>");
    	}
    })
    	.error(function(error){
    		console.log(error);
    		$("#tabella").hide();
    		$("#esami-per-anno-paragraph").hide();
    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
    		var delay = 3000; //Your delay in milliseconds
    		setTimeout(function(){ window.location.replace("generazione-esame.jsp"); }, delay);
    	});
    </script>
    <script>
    $(document).ready(function(){
    	var anniId = [];
    	var anni = [];
    	$.getJSON('webapi/esami', function(data){
    		for (var i = 0; i < data.length; i++){
    			item = data[i];
    			anniId.push(item.id.substring(0,4));
    		}
    		$.each(anniId, function(i, el){
    		    if($.inArray(el, anni) === -1) anni.push(el);
    		});
    		if(anni.length == 1){
    			$("#esami-per-anno-paragraph").hide();
    		}else{
    		for(var j = 0; j < anni.length; j++){
        		$("#esami-per-anno").append("<option value=\""+anni[j]+"\">"+anni[j]+"</option>");   		
        	}
    		$("#esami-per-anno").append("<option value=\"\" selected>Tutti</option>");
    		}
    	});
    	
    	$("#filter").click(function(){
    		var anno = $("select option:selected").val();
    		if (anno == ""){
    			$("tbody").empty();
    			$.getJSON('./webapi/esami')
    		    .done(function(data) {
    		    	for (var i = 0; i < data.length; i++) {
    		    		var stato;
    		    		var button;
    		    		var risposte;
        				var item = data[i];
        				if (item.congelato == true){
        	    			if(item.creato == true){
        	    				risposte = "<a href=\"risposte-form.jsp?esame="+item.id+"\"><button>Inserisci risposte</button></a><a href=\"risposte.jsp?esame="+item.id+"\"><button>Vai alle risposte</button></a>";
        	    				stato = "Pdf gi� creato";
        	    			}
        	    			else {
        	    				risposte = "";
        	    				stato = "Bloccato - Pdf da creare";
        	    			}
        	    			button = " <button type=\"button\" class=\"disabled\" onclick=\"editFormExam("+item.id+")\" disabled>EDIT</button>";
        	    		}
        	    		else{
        	    			stato = "Editabile";
        	    			risposte = "";
        	    			button = " <button type=\"button\" class=\"enabled\" onclick=\"editFormExam("+item.id+")\">EDIT</button>";
        	    		}
    		           		$("#tabella").append(
    		    					"<tr><td><a href=\"esame.jsp?id="+item.id+"\">" + item.id + "</a></td><td>"
    		    							+ item.data + "</td><td>"
    		    							+ item.appelloNumero + "</td><td>"+stato+"</td><td>"+risposte+"</td><td>"+button+"</td><td><button onclick=\"confirmDeteleExam("
    		    							+ item.id
    		    							+ ")\">DELETE</button></td></tr>");
    		    	}})
    		}else{
        		$.getJSON('webapi/esami?anno='+anno, function(data){
        			$("tbody").empty();
        			for (var i = 0; i < data.length; i++) {
        				var stato;
        				var button;
        				var risposte;
        				var item = data[i];
        				if (item.congelato == true){
        	    			if(item.creato == true){
        	    				risposte = "<a href=\"risposte-form.jsp?esame="+item.id+"\"><button>Inserisci risposte</button></a><a href=\"risposte.jsp?esame="+item.id+"\"><button>Vai alle risposte</button></a>";
        	    				stato = "Pdf gi� creato";
        	    			}
        	    			else {
        	    				risposte = "";
        	    				stato = "Bloccato - Pdf da creare";
        	    			}
        	    			button = " <button type=\"button\" class=\"disabled\" onclick=\"editFormExam("+item.id+")\" disabled>EDIT</button>";
        	    		}
        	    		else{
        	    			stato = "Editabile";
        	    			risposte = "";
        	    			button = " <button type=\"button\" class=\"enabled\" onclick=\"editFormExam("+item.id+")\">EDIT</button>";
        	    		}
        				$("#tabella").append(
            					"<tr><td><a href=\"esame.jsp?id="+item.id+"\">" + item.id + "</a></td><td>"
            							+ item.data + "</td><td>"
            							+ item.appelloNumero + "</td><td>"+stato+"</td><td>"+risposte+"</td><td>"+button+"</td><td><button onclick=\"confirmDeteleExam("
            							+ item.id
            							+ ")\">DELETE</button></td></tr>");
        			}
        			});
    		}
    		}
    	);
    });
    </script>
    <script type="text/javascript">
    	function changePage(dim){
    		dim++;
    		$("#tabella").empty();
    		$.getJSON('./webapi/esami?start='+dim+'&size=4')
    	    .done(function(data) {
    	    	var size = data.length;
    	    	var page;
    	    	console.log(size);
    	    	for (var i = 0; i < data.length; i++) {
    	    		var stato;
    	    		var item = data[i];
    	    		if (item.congelato == true){
    	    			stato = "Bloccato";
    	    			button = " <button type=\"button\" class=\"disabled\" onclick=\"editFormExam("+item.id+")\" disabled>EDIT</button>";
    	    		}
    	    		else{
    	    			stato = "Editabile";
    	    			button = " <button type=\"button\" class=\"enabled\" onclick=\"editFormExam("+item.id+")\">EDIT</button>";
    	    		}
    	           		$("#tabella").append(
    	    					"<tr><td><a href=\"esame.jsp?id="+item.id+"\">" + item.id + "</a></td><td>"
    	    							+ item.data + "</td><td>"
    	    							+ item.appelloNumero + "</td><td>"+stato+"</td><td><a href=\"risposte-form.jsp?esame="+item.id+"\">Inserisci risposte</a><a href=\"risposte.jsp?esame="+item.id+"\">Vai alle risposte</a></td><td>"+button+"</td><td><button onclick=\"confirmDeteleExam("
    	    							+ item.id
    	    							+ ")\">DELETE</button></td></tr>");
    	    	}
    	    })
    	    	.error(function(error){
    	    		console.log(error);
    	    		$("#tabella").hide();
    	    		$("#esami-per-anno-paragraph").hide();
    	    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
    	    		var delay = 3000; //Your delay in milliseconds
    	    		setTimeout(function(){ window.location.replace("generazione-esame.jsp"); }, delay);
    	    	});
    	}
    </script>
    <script src="assets/scripts/editExam.js"></script>
    <script src="assets/scripts/deleteExam.js"></script>
</body>
</html>