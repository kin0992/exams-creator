function deteleAnswer(esame, id) {
	$.ajax({
		method : "DELETE",
		url : "webapi/esami/" + esame + "/risposte/" + id,
		statusCode : {
			204 : function() {
			}
		}

	}).done(function() {
		location.reload();
	}).fail(function() {
		alert("Risposta " + id + " non cancellata correttamente")
	});
}

function confirmDeteleAnswer(esame, id) {
	var r = confirm("Cancellare la risposta?");
	if (r == true) {
		deteleAnswer(esame, id);
	}
}