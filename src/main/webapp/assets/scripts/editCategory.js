function editFormCategory(nome) {
			var categoria = nome.id;
			$("#modifica-categoria").empty();
			$("#tabella").hide();
			$("#categoria").hide();
			$("#modifica-categoria").show();
			$("#category-back").show();
			$.getJSON('./webapi/categorie/' + categoria, function(data) {
				item = data;
					$("#modifica-categoria").append(
							"Nome categoria: <input type=\"text\" id=\"nome\" value=\""+item.nome+"\" name =\"nome\"><br />"
							+"Descrizione : <textarea rows=\"4\" cols=\"50\" name = \"descrizione\" value = "+item.descrizione+">"+item.descrizione+"</textarea><br />"
							+"<input type=\"button\" value=\"Modifica\" onclick=\"editCategory("+item.nome+")\";>");
			});
}


function editCategory(nome) {
			var categoria = nome.id;
			var formData = $("form").serialize();
			$.ajax({
				method : "PUT",
				url : "webapi/categorie/" + categoria,
				statusCode : {
					200 : function() {
					}
				},
				data: formData
			}).done(function (){
				alert("Categorie " + categoria+ " modificata");
				window.location.replace("categorie.jsp");
			}).error(function(error){
				console.log(error);
				$("#category-back").hide();
	    		$("#modifica-categoria").hide();
	    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
			});
}
