		function deteleExam(id) {
			$.ajax({
				method : "DELETE",
				url : "webapi/esami/" + id,
				statusCode : {
					204 : function() {
						alert("Esame " + id + " cancellato.")
					}
				}
				
			}).done(function() {
				location.reload();
			})
			.fail(function() {
				alert("Esame " + id + " non cancellato correttamente")
			});
		}

		function confirmDeteleExam(id){
			var r = confirm("Cancellare l'esame "+ id + "?");
			if (r == true) {
				deteleExam(id);
			}
		}