		function deleteInfo(id) {
			$.ajax({
				method : "DELETE",
				url : "webapi/info/" + id,
				statusCode : {
					204 : function() {
						alert("Informazioni cancellate.")
					}
				}
				
			}).done(function() {
				window.location.replace("info.jsp");
			})
			.fail(function() {
				alert("Informazioni non cancellate correttamente")
			});
		}

		function confirmDeteleInfo(id){
			var r = confirm("Confermi?");
			if (r == true) {
				deleteInfo(id);
			}
		}