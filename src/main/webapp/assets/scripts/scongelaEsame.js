function sbloccaEsame(examId){
	$.ajax({
		method : "PUT",
		url : "webapi/esami/" + examId,
		statusCode : {
			201 : function() {
			}
		},
		data: {
			scongela: true
			}
	}).done(function (){
		alert("Esame " + examId+ " sbloccato");
		window.location.reload();
	}).error(function(error){
		console.log(error);
		$("#category-back").hide();
		$("#modifica-categoria").hide();
		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	});
}