function checkCategories(categorie){
	$.getJSON('./webapi/categorie', function(data) {
		for (var i = 0; i < data.length; i++) {
			var item = data[i];
			$("#categorie")
					.append(
							"<input class='categoria' type = 'checkbox' name = \"categorie\" value =\""+item.nome+"\" id=\""+item.nome+"\" >"
									+ item.nome);
			if($.inArray(item.nome, categorie) != -1){
				$("#"+item.nome).prop('checked', true);
			}
		}
	});
}