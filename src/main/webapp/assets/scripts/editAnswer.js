function editFormAnswer(esame, risposta) {
	$("#update-risposta").show();
	$("#students").hide();
	$("#tabella-risposte").hide();
	$("#exam-back").hide();
	$("#answer-back").show();
			$.getJSON('./webapi/esami/' + esame + '/risposte/' + risposta, function(data) {
				item = data;
				var rispostaData = item.rispostaData;
				if(item.domanda.aperta == true){
					$("#update-risposta").append("<th>Domanda</th><th>Punteggio base</th><th>Volte</th><th>Correttezza</th><th>Salva</th>");
					$("#update-risposta").append("<tr><td>"+item.domanda.testoDomanda+"</td><td>"+item.domanda.punteggioBase+"</td><td><input id=\"volte\" name=\"volte\" type=\"number\" value=\""+item.occorrenze+"\" min=0 max=100 \"></td><td><input id=\"correttezza\" type=\"number\" value=\""+100*item.correttezza+"\" min=0 max=100 step=5 \"></td><td><button onclick=\"editAnswer("+esame+","+risposta+");\">Salva</button></td></tr>");
				}else{
					$("#update-risposta").append("<th>Domanda</th><th>Punteggio base</th><th>Volte</th><th>Correttezza</th><th>Risposta data</th><th>Salva</th>");
					$("#update-risposta").append("<tr><td>"+item.domanda.testoDomanda+"</td><td>"+item.domanda.punteggioBase+"</td><td><input id=\"volte\" name=\"volte\" type=\"number\" value=\""+item.occorrenze+"\" min=0 max=100 \"></td><td><input id=\"correttezza\" type=\"number\" value=\""+100*item.correttezza+"\" min=0 max=100 step=5 \"></td><td>"+rispostaData+"</td><td><button onclick=\"editAnswer("+esame+","+risposta+");\">Salva</button></td></tr>");
				}
					
					
			});
}


function editAnswer(esame, risposta) {
			$.ajax({
				method : "PUT",
				url : "webapi/esami/" + esame + "/risposte/"+ risposta,
				statusCode : {
					200 : function() {
					}
				},
				data: {
					volte: $('#volte').val(),
					correttezza: $('#correttezza').val()
					}
			}).done(function (){
				alert("Risposta modificata");
				window.location.replace("risposte.jsp?esame="+esame);
			}).fail(function(error) {
				console.log(error);
			});
}
function mostraRisposte(){
	$("#answer-back").hide();
	$("#tabella-risposte").show();
	$("#exam-back").show();
	$("#update-risposta").empty();
	$("#update-risposta").hide();
}
function mostraEsami(){
	$("#answer-back").hide();
	$("#tabella-risposte").empty();
	$("#tabella-risposte").hide();
	$("#tabella").show();
	$("#exam-back").hide();
}