		function deteleCategory(nome) {
			var categ = nome.id;
			console.log(categ);
			$.ajax({
				method : "DELETE",
				url : "webapi/categorie/" + nome.id,
				statusCode : {
					204 : function() {
					}
				}
				
			}).done(function() {
				//alert("Categoria " + nome.id + " cancellata");
				window.location.reload();
			})
			.fail(function() {
				alert("Categoria " + nome.id + " non cancellata correttamente")
			});
		}

		function confirmDeteleCategory(nome){
			var r = confirm("Cancellare la categoria "+ nome.id + "?");
			if (r == true) {
				deteleCategory(nome);
			}
		}