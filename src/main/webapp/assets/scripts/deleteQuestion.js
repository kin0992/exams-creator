		function deteleQuestion(numero) {
			$.ajax({
				method : "DELETE",
				url : "webapi/domande/" + numero,
				statusCode : {
					204 : function() {
					}
				}
				
			}).done(function() {
				alert("Domanda " + numero + " cancellata correttamente");
				window.location.replace("domande.jsp");
			})
			.error(function() {
				alert("Domanda " + numero + " non cancellata correttamente")
			});
		}

		function confirmDeteleQuestion(numero){
			var r = confirm("Cancellare la domanda "+ numero + "?");
			if (r == true) {
				deteleQuestion(numero);
			}
		}