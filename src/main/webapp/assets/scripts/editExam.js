function editFormExam(examId) {
	$("#domande-aggiunte").show();
	$("#esami-per-anno-paragraph").hide();
	$("#anteprima").hide();
	$("#modifica-esame").empty();
	$("#esame").empty();
	$("#modifica-esame").append("<div id=\"numeriDomande\"></div>");
	$("#modifica-esame").append("<div id=\"loghi\"></div>");
	//$("#modifica-esame").append("<div id=\"puntiDomande\"></div>");
	$("#modifica-esame").append("<table id=\"tabDomande\" class=\"tabella\"></table>");
	$("#modifica-esame").append("<table id='domande-aggiunte' class='tabella'><caption>Domande selezionate</caption></table>");
	$("#esame").hide();
	$("#tabella").hide();
	$("#domande-presenti").show();
	$("#modifica-esame").show();
	$("#domande-aggiunte")
	.append(
			"<th>#</th><th>Domanda</th><th>Tipo</th><th>Punti</th><th id=\"appelloOrder\">Appelli</th><th id=\"counterOrder\">Contatore</th>");
	ch = [];
	pageSize=10;
	already = [];
	domAggSize = 0;
//	$.getJSON('./webapi/domande', function(data){
//		var domande = data;
//		domandeTotali = domande.length; // numero domande totali
//	});
	$.getJSON('./webapi/esami/'+examId, function(data){
		item = data;
		$("#loghi").append("Aggiungere logo bicocca?<input type='radio' name='logoBicocca' value='1'> Si");
		$("#loghi").append("<input type='radio' name='logoBicocca' value='0' checked> No <br />");
		$("#loghi").append("Aggiungere logo DISCo?<input type='radio' name='logoDisco' value='1'> Si");
		$("#loghi").append("<input type='radio' name='logoDisco' value='0' checked> No");
		punteggio = [];
		punteggio = item.puntiDomande.entry;
		asd = punteggio;
		var numeri = []; 
		domandeEsame = item.domande; // domandeEsame contiene tutte le
										// domande per quell'esame
		for(var j = 0; j < domandeEsame.length; j++){
			numeri.push(domandeEsame[j].numero);
		}
		for (var i = 0; i < domandeEsame.length; i++){
			var numero = domandeEsame[i].numero;
			already.push(numero);
			var item = domandeEsame[i];
			var tipo;
			if(item.aperta == true){
				tipo = "Aperta";
			}else{
				tipo = "Crocette";
			}
			for (var i2 = 0;i2 < punteggio.length; i2++){
				if(punteggio[i2].key == item.numero)
					punti = punteggio[i2].value;
			}
			$("#numeriDomande").append("<input type=\"checkbox\" name=\"numeroDomanda\" style=\"display:none\" id=\"checkNumero"+numero+"\" value="+numero+" checked>");
			$("#puntiDomande").append("<input type='hidden' id='punti-"+item.numero+"' name='puntiCambiati' value='"+punti+"'>");
			$("#domande-aggiunte").append("<tr><tr id=\"questionAdd"+item.numero+"\"><td><img class=\"removeQuestion\" src=\"assets/images/webapp/remove.png\" onclick=\"removeQ("+numero+")\"></td><td>"
					+ item.testoDomanda
					+ "</td><td>"+tipo+"</td><td><input type=\"number\" id=\"overridePunt\" name=\"puntiCambiati\" min = 1 value =\""+punti+"\"></td><td>"
				+ item.appelli
				+ "</td><td>"
				+ item.contatore
				+ "</td></tr>");
		}
		domAggSize = already.length;
	});
	$.getJSON('./webapi/domande?order=appello&start=1&size='+pageSize, function(data){
		domande = data;
		pages = Math.ceil(domandeTotali/pageSize);
		$("#page-number").insertAfter("#tabDomande");
		if (pages == 1){
		}else{
			for (var i = 0; i < pages; i++){
				pN = i+1;
				$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePage("+pN+");\"><u>"+pN+"</u></div>");
			}
		}
	$.getJSON('./webapi/esami/' + examId, function(data) {
		item = data;
		var numeri = []; 
		var form = $("#tabDomande");
		$("<div id=\"data\" class=\"date-appello\">Data Esame*: <input type=\"date\" id=\"data-esame\" name=\"data-esame\" value=\""+item.data+"\"></div>").insertBefore("#tabDomande");
		$("<div id=\"appello\" class=\"date-appello\">Appello numero*: <input type=\"number\" id=\"numero-appello\" name=\"appello\" value=\""+item.appelloNumero+"\" max=7 min=1></div>").insertBefore("#tabDomande");
		$("<div id=\"filter-question\"><p id=\"domande-per-categoria-paragraph\">Visualizza le domande della categoria: <select id=\"domande-per-categoria\"></select><button id=\"filter-by-category\">Filtra</button></p></div>").insertAfter("#appello");
		loadCategoryPicker();
		filterButtonFunction();
			form.append("<th>#</th><th>Domanda</th><th>Tipo</th><th>Punti</th><th>Appelli</th><th>Contatore</th>");
			for (var i = 0; i < domande.length; i++){
				var idDom = domande[i].numero;
				var tipo;
				if(domande[i].aperta == true){
					tipo = "Aperta";
				}else{
					tipo = "Crocette";
				}
				if($.inArray(idDom, already) != -1){
				}else{
				form.append(
						"<tr><tr id=\"question"+idDom+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+idDom+")\"></td><td>"
						+ domande[i].testoDomanda
						+ "</td><td>"+tipo+"</td><td>"+domande[i].punteggioBase+"</td><td>"
					+ domande[i].appelli
					+ "</td><td>"
					+ domande[i].contatore
					+ "</td></tr>");}
			}
			$("<input type=\"button\" value=\"Aggiorna esame\" onclick=\"editExam("+item.id+")\";>").insertAfter("#domande-aggiunte");
	});
	});
	
}


function addQ(numero){
	if($.inArray(numero, already) != -1){
	}else{
	already.push(numero);
	if (already.length > 0)
		$("#domande-selezionate").show();
	$("#numeriDomande").append("<input type=\"checkbox\" name=\"numeroDomanda\" style=\"display:none\" id=\"checkNumero"+numero+"\" value="+numero+" checked>");
	domAggSize++;
	$("#domande-aggiunte").show();
	$.getJSON('./webapi/domande/'+numero)
	.done(function(data){
		var item = data;
		var tipo;
		if (item.aperta == true){
			tipo = "Aperta";
		}
		else{
			tipo = "Crocette";
		} 
		$("#question"+numero).remove();
		$("#domande-aggiunte").append("<tr id=\"questionAdd"+item.numero+"\"><td><img class=\"removeQuestion\" src=\"assets/images/webapp/remove.png\" onclick=\"removeQ("+item.numero+")\"></td><td>"
				+ item.testoDomanda
				+ "</td><td>"+tipo+"</td><td><input type=\"number\" name=\"puntiCambiati\" id='punti-"+item.numero+"' value=\""+item.punteggioBase+"\" min=1></td><td>"
				+ item.appelli
				+ "</td><td>"
				+ item.contatore
				+ "</td></tr>");
		//var punti = $("#punti-"+item.numero).val();
		//$("#puntiDomande").append("<input type='hidden' id='punti-"+item.numero+"' name='puntiCambiati' value='"+punti+"'>");//se cambio punteggio non funziona
	});}
}


function removeQ(numero){
	domAggSize--;
	already = jQuery.grep(already, function(value) {
		  return value != numero;
	});
	if (already.length == 0)
		$("#domande-aggiunte").hide();
	if(domAggSize == 0)
		$("#domande-aggiunte").hide();
	$("#checkNumero"+numero).remove();
	$("#punti-"+numero).remove();
	$.getJSON('./webapi/domande/'+numero)
	.done(function(data){
		var item = data;
		var tipo;
		if (item.aperta == true){
			tipo = "Aperta";
		}
		else{
			tipo = "Crocette";
		} 
		$("#questionAdd"+numero).remove();
		$("#tabDomande").append("<tr id=\"question"+item.numero+"\"><td><img class=\"removeQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
				+ item.testoDomanda
				+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
				+ item.appelli
				+ "</td><td>"
				+ item.contatore
				+ "</td></tr>");
	});
}

function changePage(lastPage){
	var start;
	if(lastPage != 1){
		lastPage--;
		start = lastPage*pageSize;
	}
	else{
		start = 1;
	}
	$('#tabDomande tbody').empty();
	$.getJSON('./webapi/domande?order=appello&start='+start+'&size='+pageSize)
	.done(function(data){
		for (var i = 0; i < data.length; i++) {
			var item = data[i];
			var tipo;
			if (item.aperta == true){
				tipo = "Aperta";
			}
			else{
				tipo = "Crocette";
			}
			if($.inArray(item.numero, already) != -1){
			}else{
				$("#tabDomande")
				.append(
						"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
								+ item.testoDomanda
								+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
								+ item.appelli
								+ "</td><td>"
								+ item.contatore
								+ "</td></tr>");
			}
				
		}});
	}



function editExam(examId) {
	var formData = $("form").serialize();
		$.ajax({
			method : "PUT",
			url : "webapi/esami/" + examId,
			statusCode : {
				201 : function() {
				}
			},
			data: formData
		}).done(function (){
			alert("Esame " +examId+ " modificato");
			window.location.replace("esami.jsp");
		}).error(function(error) {
			alert("Codice errore: " + error.responseJSON.codice + "\nMessaggio: " +error.responseJSON.messaggio+ "\nDocumentazione: " +error.responseJSON.documentazione);
			console.log(error);
		});
}

function loadCategoryPicker(){
	$.getJSON('./webapi/categorie')
	.done(function(data){
		var categ = data;
		if (categ.length == 1){
			$("#domande-per-categoria-paragraph").hide();
		}else {
		for (var i = 0; i < categ.length; i++){
			$("#domande-per-categoria").append("<option value=\""+categ[i].nome+"\">"+categ[i].nome+"</option>");
		}
			//$("#domande-per-categoria").append("<option value=\"\" selected>Tutte</option>");
		}
	});
	
}

function filterButtonFunction(){
	$('#filter-by-category').on("click", function(e){
	    e.preventDefault();
	    var totDomande = 0;
	    $("#tabDomande tbody").empty();
	    var selected = $("#domande-per-categoria option:selected").val();
	    if (selected == ""){
	    	//window.location.replace("generazione-esame.jsp");
	    }
	    $.getJSON('./webapi/domande?categoria='+selected, function(data){
	    	totDomande = data.length;
	    });
	    $.getJSON('./webapi/domande?categoria='+selected+'&start=1&size='+pageSize)
	    .done(function(data) {
	    	$("#page-number").empty();
	    	pages = Math.ceil(totDomande/pageSize);
	    	if (pages == 1){
	    	}else{
	    		for (var i = 0; i < pages; i++){
					pN = i+1;
					$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePageByCategory("+pN+");\"><u>"+pN+"</u></div>");
				}
	    	}
	    	for (var i = 0; i < data.length; i++) {
				var item = data[i];
				var tipo;
				if (item.aperta == true)
					tipo = "Aperta";
				else
					tipo = "Crocette";
				if($.inArray(item.numero, already) != -1){
				}else{
				$("#tabDomande")
				.append(
						"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
								+ item.testoDomanda
								+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
								+ item.appelli
								+ "</td><td>"
								+ item.contatore
								+ "</td></tr>");}
			}})
		.error(function(error){
	console.log(error);
	$("#tabella").hide();
	$("#form-esame").hide();
	$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
});
	});
	
}




function changePageByCategory(lastPage){
	var start;
	var categoria = $("#domande-per-categoria").val();
	if(lastPage != 1){
		lastPage--;
		start = lastPage*pageSize;
	}
	else{
		start = 1;
	}
	$('#tabDomande tbody').empty();
	$.getJSON('./webapi/domande?categoria='+categoria+'&start='+start+'&size='+pageSize)
	.done(function(data){
		for (var i = 0; i < data.length; i++) {
			var item = data[i];
			var tipo;
			if (item.aperta == true){
				tipo = "Aperta";
			}
			else{
				tipo = "Crocette";
			}
			if($.inArray(item.numero, already) != -1){
			}else{
            $("#tabDomande").append(
            		"<tr id=\"question"+item.numero+"\"><td><img class=\"addQuestion\" src=\"assets/images/webapp/add.png\" onclick=\"addQ("+item.numero+")\"></td><td>"
					+ item.testoDomanda
					+ "</td><td>"+tipo+"</td><td>"+item.punteggioBase+"</td><td>"
					+ item.appelli
					+ "</td><td>"
					+ item.contatore
					+ "</td></tr>");
			}
		}}); 
}

function updatePresenti(examId){
	var studenti = $("#studenti-presenti").val();
	$.ajax({
		method : "PUT",
		url : "webapi/esami/" + examId,
		statusCode : {
			201 : function() {
			}
		},
		data: {
			studenti_presenti: studenti
			}
	}).done(function (){
		alert("Numero studenti presenti all'esame aggiornato con il valore "+ $("#studenti-presenti").val());
		window.location.replace("esami.jsp");
	}).error(function(error) {
		alert("Codice errore: " + error.responseJSON.codice + "\nMessaggio: " +error.responseJSON.messaggio+ "\nDocumentazione: " +error.responseJSON.documentazione);
		console.log(error);
	});
}