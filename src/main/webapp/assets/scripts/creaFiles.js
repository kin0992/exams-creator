function creaFiles(examId){
		$.ajax({
			method : "PUT",
			url : "webapi/esami/" + examId,
			statusCode : {
				201 : function() {
				}
			},
			data: {
				crea: true
				}
		}).done(function (){
			alert("Sono stati creati i file dell'esame");
			window.location.reload();
		}).error(function(error){
			console.log(error);
			$("#category-back").hide();
			$("#modifica-categoria").hide();
			$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
		});
}
