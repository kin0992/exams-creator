function editFormQuestion(numero) {
	$("#elenco-appelli").hide();
	$("#page-number").hide();
	$("#domanda").empty();
	$("#domanda").hide();
	$("#modifica-domanda").empty();
	$("#modifica-domanda").show();
	$("#tabella").hide();
	$("#category-back").show();
	$("#domande-per-categoria-paragraph").hide();
	$("#domande-per-tipo-paragraph").hide();
	var domanda = $.getJSON('./webapi/domande/' + numero, function(data){
		item = data;
		categorie = [];
		categories = item.categorie;
		for (var i = 0; i < categories.length; i++){
			categorie.push(categories[i].nome);
		}
		if(item.aperta == true)
			editOpenQuestion(item, categorie);
		else
			editCloseQuestion(item, categorie);
	});
}

function editOpenQuestion(item, categorie){
	var form = $("#modifica-domanda");
	form.append("<legend>Informazioni domanda:</legend>");
	form.append("Testo*: <textarea rows=\"4\" cols=\"50\" name=\"testo\"  required>"+item.testoDomanda+"</textarea><br/>")
	form.append("Risposta: <input type=\"text\" name=\"risposta\" value =\""+item.rispostaDomanda+"\"> <br />");
	form.append("Immagine: <input name=\"immagine\" type=\"file\" size=\"20\" id=\"immagine\" accept=\"image/*\"><br />");
	form.append("Spazi bianchi dopo il testo della domanda: <input type=\"number\" min=0 name=\"spazi-bianchi\" value=\""+item.spaziBianchi+"\">");
	form.append("<br/> Punteggio Base: <input type=\"number\" name=\"punteggio-base\" min=\"1\" max=\"5\" value =\""+item.punteggioBase+"\"> <br />");
	form.append("<div id=\"categorie\">Categorie*: </div>");
	form.append("<input type = 'button' id='submit' value ='MODIFICA'>");
	form.append("<div id=\"no-category\">Categoria non presente? Clicca <a href=\"categorie-form.jsp\">qui</a> per aggiungerne una nuova.</div>");
	editQuestion(item.numero);
	checkCategories(categorie);
}

function editCloseQuestion(item, categorie){
	var form = $("#modifica-domanda");
	form.append("<legend>Informazioni domanda:</legend>");
	form.append("Testo*: <textarea rows=\"4\" cols=\"50\" name=\"testo\"  required>"+item.testoDomanda+"</textarea><br/>")
	form.append("<br/> Punteggio Base: <input type=\"number\" name=\"punteggio-base\" min=\"1\" max=\"5\" value =\""+item.punteggioBase+"\"> <br />");
	form.append("<div id=\"opzioni\">Opzioni*: ");
	form.append("Risposta corretta*: <select name=\"risposta\" id=\"risposta\"></select> <br />");
	form.append("<div id=\"categorie\">Categorie*: </div>");
	form.append("<input type = 'button' id='submit' value ='MODIFICA' >");
	form.append("<div id=\"no-category\">Categoria non presente? Clicca <a href=\"categorie-form.jsp\">qui</a> per aggiungerne una nuova.</div>");
	caricaOpzioni(item.numero);
	checkCategories(categorie);
	editQuestion(item.numero);
}
function editQuestion(numero) {
	$("#submit").click(function(){
        var form = $(this).parents("form")[0];
        var formData = new FormData(form);
        $.ajax({
       	 url: "webapi/domande/" + numero, 
            type: 'PUT',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
			 data : formData,
			 cache: false,
		     contentType: false,
		     processData: false,
       	 success: function(event){
       	 	console.log(event);
       	 	window.location.replace("domanda.jsp?id="+numero);
        	 }
        });
        return false;
   });
}