<html>
<head>
<title>Domande</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
</head>
<body>
	 <p id="domande-per-categoria-paragraph">Visualizza le domande della categoria: <select id="domande-per-categoria"></select><button id="filter-by-category">Filtra</button></p>
	  <p id="domande-per-tipo-paragraph">Visualizza le domande per tipo: <select id="domande-per-tipo"><option value="aperte">Aperte</option><option value="chiuse">Chiuse</option><option value="">Tutte</option></select><button id="filter-by-type">Filtra</button></p>
	<table id="tabella" class="tabella"></table>
	<div id="page-number"></div>
	<form id="modifica-domanda" enctype="multipart/form-data" accept-charset=utf-8></form>
	<div class="error-message"></div>
	<script>
	$("#question-back").hide();
		$("#tabella")
				.append(
						"<th>Numero</th><th>Domanda</th><th>Contatore</th><th>Edit</th><th>Delete</th>");
	</script>
	<script type="text/javascript">
	var domandeTotali;
	$.getJSON('./webapi/domande')
	.done(function(data){
		domandeTotali = data.length;
	tipo = "${param.tipo}";
	pageSize = 15;
	categoria = "${param.categoria}";
	if(categoria == "" || categoria == null){
		if (tipo == "" || tipo == null){
			$.getJSON( './webapi/domande?start=1&size='+pageSize)
			.done(function(data) {
				pages = Math.ceil(domandeTotali/pageSize);
				if (pages == 1){
				}else{
					for (var i = 0; i < pages; i++){
						pN = i+1;
						$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePage("+pN+");\"><u>"+pN+"</u></div>");
					}
				}
	    		for (var i = 0; i < data.length; i++) {
					var item = data[i];
	                $("#tabella").append(
							"<tr><td><a href=\"domanda.jsp?id="+item.numero+"\">" + item.numero + "</a></td><td>"
									+ item.testoDomanda + "</td><td>"
									+ item.contatore + "</td><td><button onclick=\"editFormQuestion("
									+ item.numero
									+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
									+ item.numero
									+ ")\">DELETE</button></td></tr>");
	    		}})
	    		.error(function(error){
	    			console.log(error);
	        		$("#tabella").hide();
	        		$("#domande-per-categoria-paragraph").hide();
	        		$("#domande-per-tipo-paragraph").hide();
	        		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	        		var delay = 3000; //Your delay in milliseconds
		    		setTimeout(function(){ window.location.replace("domande-form.jsp"); }, delay);
	    		})
		}else{
			$.getJSON('./webapi/domande?type='+tipo, function (data){
				domandeTotali = data.length;
			});
			$.getJSON( './webapi/domande?type='+tipo+'&start=1&size='+pageSize)
			.done(function(data) {
				pages = Math.ceil(domandeTotali/pageSize);
				if (pages == 1){
				}else{
					for (var i = 0; i < pages; i++){
						pN = i+1;
						$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePageByType("+pN+");\"><u>"+pN+"</u></div>");
					}
				}
	    		for (var i = 0; i < data.length; i++) {
					var item = data[i];
					for (var i = 0; i < data.length; i++) {
						var item = data[i];
		                $("#tabella").append(
								"<tr><td><a href=\"domanda.jsp?id="+item.numero+"\">" + item.numero + "</a></td><td>"
										+ item.testoDomanda + "</td><td>"
										+ item.contatore + "</td><td><button onclick=\"editFormQuestion("
										+ item.numero
										+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
										+ item.numero
										+ ")\">DELETE</button></td></tr>");
		    		}}})
	    		.error(function(error){
	    			console.log(error);
	        		$("#tabella").hide();
	        		$("#domande-per-categoria-paragraph").hide();
	        		$("#domande-per-tipo-paragraph").hide();
	        		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+" per la categoria selezionata.<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	        		var delay = 3000; //Your delay in milliseconds
		    		setTimeout(function(){ window.location.replace("domande.jsp"); }, delay);
	    		})
		}
	}else{
		$.getJSON( './webapi/domande?categoria='+categoria, function(data){
			domandeTotali = data.length;
		});
		$.getJSON( './webapi/domande?categoria='+categoria+'&start=1&size='+pageSize)
		.done(function(data) {
			pages = Math.ceil(domandeTotali/pageSize);
			if (pages == 1){
			}else{
				for (var i = 0; i < pages; i++){
					pN = i+1;
					$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePageByCategory("+pN+");\"><u>"+pN+"</u></div>");
				}
			}
    		for (var i = 0; i < data.length; i++) {
				var item = data[i];
				for (var i = 0; i < data.length; i++) {
					var item = data[i];
	                $("#tabella").append(
							"<tr><td><a href=\"domanda.jsp?id="+item.numero+"\">" + item.numero + "</a></td><td>"
									+ item.testoDomanda + "</td><td>"
									+ item.contatore + "</td><td><button onclick=\"editFormQuestion("
									+ item.numero
									+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
									+ item.numero
									+ ")\">DELETE</button></td></tr>");
	    		}}})
    		.error(function(error){
    			console.log(error);
        		$("#tabella").hide();
        		$("#domande-per-categoria-paragraph").hide();
        		$("#domande-per-tipo-paragraph").hide();
        		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+" per la categoria selezionata.<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
        		var delay = 3000; //Your delay in milliseconds
	    		setTimeout(function(){ window.location.replace("domande.jsp"); }, delay);
    		})
	}
	}).error(function(error){
		console.log(error);
		$("#tabella").hide();
		$("#domande-per-categoria-paragraph").hide();
		$("#domande-per-tipo-paragraph").hide();
		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
		var delay = 3000; //Your delay in milliseconds
		setTimeout(function(){ window.location.replace("domande-form.jsp"); }, delay);
	})
		
	</script>
	<script>
		$(document).ready(function(){//carica categorie nel tag select
			$.getJSON('./webapi/categorie', function(data){
				var categ = data;
				if (categ.length == 1){
					$("#domande-per-categoria-paragraph").hide();
				}else {
				for (var i = 0; i < categ.length; i++){
					$("#domande-per-categoria").append("<option value=\""+categ[i].nome+"\">"+categ[i].nome+"</option>");
				}
					$("#domande-per-categoria").append("<option value=\"\" selected>Tutte</option>");
					$("#filter-by-category").click(function(){
						var selected = $("#domande-per-categoria option:selected").val();
						if (selected == ""){
							window.location.replace("domande.jsp");
						}else{
							window.location.replace("domande.jsp?categoria="+selected);
						}
					});
					$("#filter-by-type").click(function(){
						var selected = $("#domande-per-tipo option:selected").val();
						if (selected == "" || selected == "Tutte"){
							window.location.replace("domande.jsp");
						}else{
							window.location.replace("domande.jsp?tipo="+selected);
						}
					});
				}
				
			});
		});
	</script>
	<script>
		function changePage(lastPage){
			var start;
			if(lastPage != 1){
				lastPage--;
				start = lastPage*pageSize;
			}
			else{
				start = 1;
			}
			$('tbody').empty();
			$.getJSON('./webapi/domande?start='+start+'&size='+pageSize)
			.done(function(data){
				for (var i = 0; i < data.length; i++) {
					var item = data[i];
	                $("#tabella").append(
							"<tr><td><a href=\"domanda.jsp?id="+item.numero+"\">" + item.numero + "</a></td><td>"
									+ item.testoDomanda + "</td><td>"
									+ item.contatore + "</td><td><button onclick=\"editFormQuestion("
									+ item.numero
									+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
									+ item.numero
									+ ")\">DELETE</button></td></tr>");
	    		}}).error(function(error){
	    			console.log(error);
	        		$("#tabella").hide();
	        		$("#domande-per-categoria-paragraph").hide();
	        		$("#domande-per-tipo-paragraph").hide();
	        		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	        		var delay = 3000; //Your delay in milliseconds
		    		setTimeout(function(){ window.location.replace("domande-form.jsp"); }, delay);
	    		});
			}
	</script>
	<script>
	function changePageByType(lastPage){
		var start;
		var type = tipo;
		if(lastPage != 1){
			lastPage--;
			start = lastPage*pageSize;
		}
		else{
			start = 1;
		}
		$('tbody').empty();
		$.getJSON('./webapi/domande?type='+type+'&start='+start+'&size='+pageSize)
		.done(function(data){
			for (var i = 0; i < data.length; i++) {
				var item = data[i];
                $("#tabella").append(
						"<tr><td><a href=\"domanda.jsp?id="+item.numero+"\">" + item.numero + "</a></td><td>"
								+ item.testoDomanda + "</td><td>"
								+ item.contatore + "</td><td><button onclick=\"editFormQuestion("
								+ item.numero
								+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
								+ item.numero
								+ ")\">DELETE</button></td></tr>");
    		}}).error(function(error){
    			console.log(error);
        		$("#tabella").hide();
        		$("#domande-per-categoria-paragraph").hide();
        		$("#domande-per-tipo-paragraph").hide();
        		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
        		var delay = 3000; //Your delay in milliseconds
	    		setTimeout(function(){ window.location.replace("domande-form.jsp"); }, delay);
    		})
		}
	</script>
	
	<script>
	function changePageByCategory(lastPage){
		var start;
		var category = categoria;
		if(lastPage != 1){
			lastPage--;
			start = lastPage*pageSize;
		}
		else{
			start = 1;
		}
		$('tbody').empty();
		$.getJSON('./webapi/domande?categoria='+categoria+'&start='+start+'&size='+pageSize)
		.done(function(data){
			for (var i = 0; i < data.length; i++) {
				var item = data[i];
                $("#tabella").append(
						"<tr><td><a href=\"domanda.jsp?id="+item.numero+"\">" + item.numero + "</a></td><td>"
								+ item.testoDomanda + "</td><td>"
								+ item.contatore + "</td><td><button onclick=\"editFormQuestion("
								+ item.numero
								+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
								+ item.numero
								+ ")\">DELETE</button></td></tr>");
    		}})
    		.error(function(error){
	    			console.log(error);
	        		$("#tabella").hide();
	        		$("#domande-per-categoria-paragraph").hide();
	        		$("#domande-per-tipo-paragraph").hide();
	        		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	        		var delay = 3000; //Your delay in milliseconds
		    		setTimeout(function(){ window.location.replace("domande-form.jsp"); }, delay);
	    		})
		}
	</script>
	<script src="assets/scripts/loadAndCheckCategories.js"></script>
	<script src="assets/scripts/checkCategory.js"></script>
	<script src="assets/scripts/caricaOpzioni.js"></script>
	<script src="assets/scripts/editQuestion.js"></script>
	<script src="assets/scripts/deleteQuestion.js"></script>
</body>
</html>