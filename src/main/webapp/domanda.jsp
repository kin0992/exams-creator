<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<jsp:include page="/includes/menu.jsp"></jsp:include>
<p id ="intestazione"></p>
<form id="modifica-domanda" enctype="multipart/form-data" accept-charset=utf-8></form>
<div class="error-message"></div>
<table id="domanda" class="tabella"></table>
<div id="elenco-appelli"></div>
<script type="text/javascript">

var idDomanda = "${param.id}";
var categ = [];
var opzioni = [];
$.getJSON('./webapi/domande/'+idDomanda, function(data){
	$.each(data.categorie, function(nome, descriz) {
		categ.push(descriz.nome);
	});
	$("#intestazione").append("Domanda numero: " + idDomanda + ".");
	if(data.aperta == true){
		if(data.immagineDomanda == ""){
			$("#domanda")
			.append(
					"<th>Testo</th><th>Risposta</th><th>Categorie</th><th>Punteggio</th><th>Contatore</th><th>Edit</th><th>Delete</th>");
			$("#domanda").append("<tr><td>"+data.testoDomanda+"</td><td>"+data.rispostaDomanda+"</td><td><ul id=\"categ\"></ul></td><td>"+data.punteggioBase+"</td><td>"+data.contatore+"</td><td><button onclick=\"editFormQuestion("
					+ idDomanda
					+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
					+ idDomanda
					+ ")\">DELETE</button></td></tr>");
			if(data.contatore !=  0){
				$("#elenco-appelli").append("Appelli: "+data.appelli);			
			}
		}else{
			$("#domanda")
			.append(
					"<th>Testo</th><th>Risposta</th><th>Categorie</th><th>Punteggio</th><th>Immagine</th><th>Contatore</th><th>Edit</th><th>Delete</th>");
			$("#domanda").append("<tr><td>"+data.testoDomanda+"</td><td>"+data.rispostaDomanda+"</td><td><ul id=\"categ\"></ul></td><td>"+data.punteggioBase+"</td><td><a href=\"assets/images/domande/"+data.immagineDomanda+"\">Show Image</a><div class=\"box\"><iframe src=\"assets/images/domande/"+data.immagineDomanda+"\" width = \"500px\" height = \"500px\"></iframe></div></td><td>"+data.contatore+"</td><td><button onclick=\"editFormQuestion("
					+ idDomanda
					+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
					+ idDomanda
					+ ")\">DELETE</button></td></tr>");
			if(data.contatore !=  0){
				$("#elenco-appelli").append("Appelli: "+data.appelli);			
			}
		}
	}else{
		$("#domanda")
		.append(
				"<th>Testo</th><th>Risposta</th><th>Categorie</th><th>Punteggio</th><th>Contatore</th><th>Opzioni</th><th>Edit</th><th>Delete</th>");
		$("#domanda").append("<tr><td>"+data.testoDomanda+"</td><td>"+data.rispostaDomanda+"</td><td><ul id=\"categ\"></ul></td><td>"+data.punteggioBase+"</td><td>"+data.contatore+"</td><td><ul id=\"opzRisp\"></ul></td><td><button onclick=\"editFormQuestion("
				+ idDomanda
				+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleQuestion("
				+ idDomanda
				+ ")\">DELETE</button></td></tr>");
		loadOptions();
		if(data.contatore !=  0){
			$("#elenco-appelli").append("Appelli: "+data.appelli);			
		}
	}
	
});
</script>
<script>
$.getJSON('./webapi/domande/'+idDomanda, function(data){
	for(var i = 0; i < data.categorie.length; i++){
		$("#categ").append("<li><a href=\"domande.jsp?categoria="+data.categorie[i].nome+"\">"+data.categorie[i].nome+"</a></li>");	
	}
});
function loadOptions(){
	$.getJSON('./webapi/domande/'+idDomanda, function(data){
		for(var i = 0; i < data.opzioniRisposta.length; i++){
			$("#opzRisp").append("<li>"+data.opzioniRisposta[i]+"</li>");	
		}
	});
	
}

</script>

<script src="assets/scripts/editQuestion.js"></script>
<script src="assets/scripts/deleteQuestion.js"></script>
<script src="assets/scripts/loadAndCheckCategories.js"></script>
<script src="assets/scripts/checkCategory.js"></script>
<script src="assets/scripts/caricaOpzioni.js"></script>
</body>
</html>