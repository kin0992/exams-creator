<html>
<head>
<title>Informazioni</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
<div id="informazioni"></div>
<form id="informazioni-esame" action="webapi/info" accept-charset=utf-8></form>
<script>
var returnTo = "${param.from}";
$.getScript("assets/scripts/deleteInfo.js");
$.getJSON('./webapi/info', function(data) {
	var item = data[0];
	var id = item.id;
	var div = $("#informazioni-esame");
	div.append("<p>UniversitÓ: <input type=\"text\" name=\"uni\" id=\"uni\" value=\""+item.uni+"\"></p>");
	div.append("<p>Corso di laurea: <input type=\"text\" name=\"corso-laurea\" id=\"corso-laurea\" value=\""+item.cDL+"\"></p>");
	div.append("<p>Anno accademico: <input type=\"text\" name=\"anno-accademico\" id=\"anno-accademico\" value=\""+item.annoAccademico+"\"></p>");
	div.append("<p>Nome corso: <input type=\"text\" name=\"nome-corso\" id=\"nome-corso\" value=\"" +item.nomeCorso+"\"></p>");
	div.append("<p>Regole esame: <textarea name=\"regole\" rows=\"4\" cols=\"50\">"+item.regoleEsame+"</textarea></p>");
	div.append("<button id=\"update-info\">AGGIORNA</button>");
	div.append("<button onclick=\"confirmDeteleInfo("+id+")\">DELETE</button>");
	updateInfo(id);
});

</script>
<script>
function updateInfo(id){
	$("#update-info").click(function(event){
		event.preventDefault();
		var formData = $("form").serialize();
		$.ajax({
			method : "PUT",
			url : "webapi/info/"+id,
			statusCode : {
				200 : function() {
				}
			},
			data: formData
		}).done(function (){
			alert("Informazioni modificate.");
			if (returnTo == "" || returnTo == null){
				window.location.replace("info.jsp");
			}else
				{
				window.location.replace(returnTo);
				}
		});
	});
	
}
</script>
</body>
</html>
