<html>
<head>
<title>Risposte</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
</head>
<body>
<p id="intestazione"></p>
	<table id="tabella-risposte" class="tabella">
	</table>
	<div id="page-number"></div>
	<table id="update-risposta" class="tabella">
	</table>
	<div id="students"></div>
	<div class="error-message"></div>
	<script>
	var exam = "${param.esame}";
	risposteTotali = 0;
	if(exam == "" || exam == null){
		window.location.replace("risposte-form.jsp");
	}else{
		$.getJSON('./webapi/esami/' + exam + '/risposte', function(data){
			risposteTotali = data.length;
		});
		pageSize = 15;
		$.getJSON('./webapi/esami/' + exam + '/risposte?start=1&size='+pageSize)
				.done(function(esame) {
					totInserite = 0;
					pages =  Math.ceil(risposteTotali/pageSize);
					if (pages == 1){
					}else{
						for (var i = 0; i < pages; i++){
							pN = i+1;
							$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePage("+pN+");\"><u>"+pN+"</u></div>");
						}
					}
					var data = esame[0].esame.data;
					$("#intestazione").append("Risposte dell'esame con data " + data);
							$("#tabella-risposte").show();
							$.getScript('assets/scripts/editAnswer.js');
							$("#tabella").hide();
							var form = $("#tabella-risposte");
							$("#tabella-risposte")
									.append(
											"<th>Domanda</th><th>Punteggio base domanda</th><th>Correttezza</th><th>Punteggio</th><th>Volte</th><th>Risposta</th><th>Edit</th><th>Delete</th>");
							for (var i = 0; i < esame.length; i++) {
								var risposta;
								if (esame[i].domanda.aperta == true){
									risposta = "";
								}
								else{
									risposta = esame[i].rispostaData;
								}
								totInserite = totInserite + esame[i].occorrenze;
								form.append("<tr><td>"
												+ esame[i].domanda.testoDomanda
												+ "</td><td>"
												+ esame[i].domanda.punteggioBase
												+ "</td><td>"
												+ esame[i].correttezza
												+ "</td><td>"
												+ esame[i].punteggio
												+ "</td><td>"+esame[i].occorrenze+"</td><td>"+risposta+"</td><td><button onclick=\"editFormAnswer("
												+ esame[i].esame.id+ ","+ esame[i].id + ");\">EDIT</button></td><td><button onclick=\"confirmDeteleAnswer("
												+ esame[i].esame.id+ ","+ esame[i].id + ");\">DELETE</button></td></tr>");
							}/*
							var presenti = esame[0].esame.studentiPresenti;
							if (presenti == 0){
								$("#students").append("Totale risposte inserite: "+ totInserite + "<br />");
								$("#students").append("Situazione attuale studenti presenti per il seguente esame: "+ presenti + "<br />");
								$("#students").append("Aggiornare gli studenti presenti all'esame con il seguente valore ("+totInserite+") ?<button id=\"update-presenti\">Aggiorna</button>");
							}else if(presenti < totInserite){
								$("#students").append("ATTENZIONE: gli studenti presenti all'esame sono "+presenti + ", mentre le risposte inserite sono "+totInserite+"<br />");
							}
							
							else{
								$("#students").append("Totale risposte inserite: "+ totInserite + "<br />");
								$("#students").append("Situazione attuale studenti presenti per il seguente esame: "+ presenti + "<br />");
							}*/
							aggiorna();

						}).error(function(error) {
							console.log(error);
				    		$("#tabella").hide();
				    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
				    		var delay = 3000; //Your delay in milliseconds
				    		setTimeout(function(){ window.location.replace("esami.jsp"); }, delay);
				});
	}
	
	function aggiorna(){
		$("#update-presenti").click(function(){
			$.ajax({
				method : "PUT",
				url : "webapi/esami/" + exam,
				statusCode : {
					201 : function() {
					}
				},
				data: {
					studenti_presenti: totInserite
					}
			}).done(function (){
				alert("Numero studenti presenti all'esame aggiornato con il valore "+ totInserite);
				window.location.replace("esami.jsp");
			}).error(function(error) {
				alert("Codice errore: " + error.responseJSON.codice + "\nMessaggio: " +error.responseJSON.messaggio+ "\nDocumentazione: " +error.responseJSON.documentazione);
				console.log(error);
			});
		});
	}
	</script>
	<script>
	function changePage(lastPage){
		var start;
		if(lastPage != 1){
			lastPage--;
			start = lastPage*pageSize;
		}
		else{
			start = 1;
		}
		$('#tabella-risposte tbody').empty();
		$.getJSON('./webapi/esami/'+exam+'/risposte?start='+start+'&size='+pageSize)
		.done(function(data){
			for (var i = 0; i < data.length; i++) {
				var item = data[i];
				var risposta;
				var form = $("#tabella-risposte");
				if (item.domanda.aperta == true){
					risposta = "";
				}
				else{
					risposta = item.rispostaData;
				}
				form.append("<tr><td>"
								+ item.domanda.testoDomanda
								+ "</td><td>"
								+ item.domanda.punteggioBase
								+ "</td><td>"
								+ item.correttezza
								+ "</td><td>"
								+ item.punteggio
								+ "</td><td>"+item.occorrenze+"</td><td>"+risposta+"</td><td><button onclick=\"editFormAnswer("
								+ item.esame.id+ ","+ item.id + ");\">EDIT</button></td><td><button onclick=\"confirmDeteleAnswer("
								+ item.esame.id+ ","+ item.id + ");\">DELETE</button></td></tr>");
			}
		});
	}
	</script>
		
	<script src="assets/scripts/deleteAnswer.js"></script>
</body>
</html>