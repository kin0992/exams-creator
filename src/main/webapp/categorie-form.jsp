<html>
<head>
<title>Inserimento Categorie</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
</head>
<body>
<div class="error-message"></div>
	<form action="webapi/categorie" method="POST" id="form-categoria">
		Informazioni categoria:<br />
		<label>Nome categoria*: </label><br /> <input type="text" name="nome" id ="nome-categoria" required autofocus> <br>
		<label>Descrizione categoria: </label><br />
		<textarea rows="4" cols="50" name="descrizione" form="form-categoria" placeholder="Inserire descrizione qui."></textarea>
		<br /> <input type="button" id="submit" value="Inserisci categoria"> <input type="reset" value="Reset">
	</form>
<jsp:include page="/includes/clearContent.jsp"></jsp:include>
<script>
var returnTo = "${param.from}";
          $("#submit").click(function(){
             var form = $(this).parents("form");
             var data = $(form).serialize();
             $.post($(form).attr("action"), data, function(event){
            	 console.log(event);
            	 if(returnTo == "" || returnTo == null) 
            	 window.location.replace("categorie.jsp");
            	 else
            		 window.location.replace(returnTo);
             })
            .error(function(error){
							console.log(error);
				    		$("#form-categoria").hide();
				    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
						});
             return false;
           });
</script>
</body>
</html>
