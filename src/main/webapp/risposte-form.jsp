<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inserimento risposte</title>
</head>
<body>
<div class="error-message"></div>
<p id="intestazione"></p>
	<form action="webapi/esami" method="POST" id="form-risposta2">
	<table id="form-risposta"></table></form>
	<script>
	iterator = 0;
	var exam = "${param.esame}";
	if (exam == null || exam == ""){
		window.location.replace("esami.jsp")
	}else{
		var action = $("#form-risposta2").attr("action");
		$("#form-risposta2").attr("action", action + "/" + exam + "/risposte");
		$("#form-risposta")
				.append(
						"<th></th><th>Testo domanda</th><th>Inserisci risposte</th>");
		$.getJSON('./webapi/esami/' + exam)
		.done(function(data) {
			$("#intestazione").append("Inserimento risposte per l'esame con data "+data.data+"<br />");
							var domande = data.domande;
							for (var i = 0; i < domande.length; i++) {
								$("#form-risposta")
										.append(
												"<tr><td id=\"#domanda\"><input type=\"hidden\" value=\""+domande[i].numero+" name =\"domanda\">"
														+ "</td><td>"
														+ domande[i].testoDomanda
														+ "</td><td><input type=\"button\" id=\"insert\" onclick=\"formAnswer("+exam+ ", "+domande[i].numero+");\" value=\"Inserisci risposte\"></td></tr>");
							}
						});
	}
	</script>
	<script type="text/javascript">
	function formAnswer(examId, domandaNum){
		contatore = 0;
		$("#form-risposta").empty();
		var form = $("#form-risposta");
		$.getJSON('./webapi/esami/' + examId)
		.done(function(data){
			var domanda;
			var domande = data.domande;
			for (var i = 0; i < domande.length; i++){
				if (domande[i].numero == domandaNum)
					domanda = domande[i];
			}
			$("#intestazione").append("Testo della domanda : " + domanda.testoDomanda + "<br />");
			if(domanda.aperta == true){
				form.append("<th></th><th>N. Volte</th><th>Correttezza in %</th>");
				form.append("<tr><td><input type=\"hidden\" value=\""+domanda.numero+"\" name = \"domanda\" ></td><td><input type=\"number\" id=\"repeat\" min=0 max=500 value=1 name=\"volte\" required></td><td><input type=\"number\" id=\"correttezza\" name=\"correttezza\" step=5 min=0 max=100 value=50 required></td></tr>");
			}else{
				form.append("<th></th><th>N. Volte</th><th>Correttezza in %</th><th>Opzioni</th>");
				opzioni = domanda.opzioniRisposta;
				for (var i = 0; i < opzioni.length; i++){
					appendiRiga(i, examId, domandaNum);
					/*
						form.append("<tr id='opzione"+num+"'><td><input type='hidden' value='"+domanda.numero+"' name = 'domanda' class='disabled-"+num+"'></td><td id='volte-"+num+"'></td><td id='correttezza-"+num+"'></td><td id='radio-"+num+"'></td></tr>");
						var occorrenze = 0;
						var correttezza = 0;
						var opzioneScelta = opzAttuale;
						$('#volte-'+num).append("<input type='number' id='repeat' min=0 max=500 name='volte' required class='disabled-"+num+"' value='"+occorrenze+"'>");
						$('#correttezza-'+num).append("<input type='number' id='correttezza' name='correttezza' min=0 max=100 value='"+correttezza+"' required class='disabled-"+num+"'>");
						$('#radio-'+num).append("<input type='radio' id='"+num+"' name='rispostaData' value='"+opzioneScelta+"'>"+opzioneScelta);
						$(".disabled-"+num).prop("disabled", true);*/
						
				}/*
				$('input[name="rispostaData"]').on('change', function() {
					console.log(this.id);
					/*
					var id = this.id;
					console.log(id);
					var x = $("input[name='rispostaData']");
					var length = x.length;
					for (var i = 0; i < length; i++){
						var num = i + 1;
						$(".disabled-"+num).prop("disabled", true);
					}
					$(".disabled-"+id).prop("disabled", false);
				});
					/*
					var valore = $(":radio[value="+this.value+"]").val();
					console.log(valore);
					$.getJSON('./webapi/esami/'+examId+'/risposte?rispostaData='+valore, function(data){
						var item = data[0];
						console.log(item);
						var occorrenze = item.occorrenze;
						var correttezza = item.correttezza*100;
						$('#volte-'+id).empty();
						$('#volte-'+id).append("<input type='number' id='repeat' min=0 max=500 name='volte' required class='disabled-"+id+"' value='"+occorrenze+"'>");
						$('#correttezza-'+id).empty();
						$('#correttezza-'+id).append("<input type='number' id='correttezza' name='correttezza' min=0 max=100 value='"+correttezza+"' required class='disabled-"+id+"'>");
					});
					$(".disabled-"+id).prop("disabled", false);*/
			}
			$("<input type='submit' value='Aggiorna' id='submit'>").insertAfter("#form-risposta");
			$("<div id='fattoLabel'></div>").insertAfter("#submit");
			postAnswer(domanda.aperta);
		})
		.error(function(error){
			$("#tabella").hide();
	   		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
	});
	}
	
	function appendiRiga(i, esame, domandaNum){
		var num = i + 1;
		var form = $("#form-risposta");
		form.append("<tr id='opzione"+num+"'><td><input type='hidden' value='"+domandaNum+"' name = 'domanda' class='disabled-"+num+"'></td><td id='volte-"+num+"'></td><td id='correttezza-"+num+"'></td><td id='radio-"+num+"'></td></tr>");
		$.getJSON('./webapi/esami/'+esame+'/risposte?rispostaData='+opzioni[i])
		.done(function (data){
			var item = data[0];
			var occorrenze = item.occorrenze;
			var correttezza = item.correttezza*100;
			var opzioneScelta = opzioni[i];
			$('#volte-'+num).append("<input type='number' id='repeat' min=0 max=500 name='volte' required class='disabled-"+num+"' value='"+occorrenze+"'>");
			$('#correttezza-'+num).append("<input type='number' id='correttezza' name='correttezza' min=0 max=100 value='"+correttezza+"' required class='disabled-"+num+"'>");
			$('#radio-'+num).append("<input type='radio' id='"+num+"' name='rispostaData' value='"+opzioneScelta+"' onchange='gestisciRadio("+num+");'>"+opzioneScelta);
			$(".disabled-"+num).prop("disabled", true);
		})
		.error(function(error){
			var occorrenze = 0;
			var correttezza = 0;
			var opzioneScelta = opzioni[i];
			$('#volte-'+num).append("<input type='number' id='repeat' min=0 max=500 name='volte' required class='disabled-"+num+"' value='"+occorrenze+"'>");
			$('#correttezza-'+num).append("<input type='number' id='correttezza' name='correttezza' min=0 max=100 value='"+correttezza+"' required class='disabled-"+num+"'>");
			$('#radio-'+num).append("<input type='radio' id='"+num+"' name='rispostaData' value='"+opzioneScelta+"' onchange='gestisciRadio("+num+");'>"+opzioneScelta);
			$(".disabled-"+num).prop("disabled", true);
		});
	}
	
	function gestisciRadio(id){
		$("#fattoLabel").html("");
		var x = $("input[name='rispostaData']");
		var length = x.length;
		for (var i = 0; i < length; i++){
			var num = i + 1;
			$(".disabled-"+num).prop("disabled", true);
		}
		$(".disabled-"+id).prop("disabled", false);
	}
	</script>
	<script>
	function postAnswer(aperta){
		console.log(aperta);
		$("#submit").click(function(){
			var form = $(this).parents("form");
	        var formData = $(form).serialize();
	        var volte = $("#repeat").val();
	        	 $.ajax({
	               	 url: $(form).attr("action"), 
	                    type: 'POST',
	    				 data : formData,
	               	 success: function(event){
	               	 	console.log(event);
	               	 	if (aperta == true){
	               	 	$("#fattoLabel").html("INFORMAZIONI SALVATE");
	               	 	$("<button id='addAgain'>Aggiungi altro</button>").insertAfter("#submit");
	               	 	$("table input[type=number]").prop('disabled', true);
	               	 	addAgain();
	               	 	}else{
	               	 	$("#fattoLabel").html("INFORMAZIONI SALVATE");
	               	 	$("table input[type=number]").prop('disabled', true);
	               	 	}
	                	 }
	                }).error(function(error){
	                	alert("Errore: controllare di aver inserito tutti i campi");
	                });
            return false;
       });
	}
	
	function addAgain(){
		$("#addAgain").click(function(event){
			event.preventDefault();
			$("#fattoLabel").html("");
			$("table input[type=number]").prop('disabled', false);
			$("#addAgain").remove();
		});
	}
		</script>
</body>
</html>