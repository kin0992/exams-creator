<html>
<head>
<title>Categorie</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
</head>
<body>
<h1>Categorie</h1>
	<jsp:include page="/includes/menu.jsp"></jsp:include>
	<table id="tabella" class="tabella">
	</table>
	<div id="page-number"></div>
	<form id="modifica-categoria"></form>
	<div class="error-message"></div>
	<script>
	$("#category-back").hide();
		$("#tabella")
				.append(
						"<th>Nome</th><th>Descrizione</th><th>Edit</th><th>Delete</th>");
	</script>
	<script type="text/javascript">
	tatCateg = 0;
	pageSize = 15;
	$.getJSON('./webapi/categorie', function(data){
		totCateg = data.length;
	});
		$.getJSON('./webapi/categorie?start=1&size='+pageSize)
		.done(function(data) {
			pages = Math.ceil(totCateg/pageSize);
			if (pages == 1){
			}else{
				for (var i = 0; i < pages; i++){
					pN = i+1;
					$("#page-number").append("<div class=\"pagesN\" id=\"page"+pN+"\" onclick=\"changePage("+pN+");\"><u>"+pN+"</u></div>");
				}
			}
							for (var i = 0; i < data.length; i++) {
								var item = data[i];
								$("#tabella")
										.append(
												"<tr id=\""+item.nome+"\"><td><a href=\"categoria.jsp?id="+item.nome+"\">"
														+ item.nome
														+ "</a></td><td>"
														+ item.descrizione
														+ "</td><td><button onclick=\"editFormCategory("
														+ item.nome
														+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleCategory("
														+ item.nome
														+ ")\">DELETE</button></td></tr>");
							}
						})
						.error(function(error){
							console.log(error);
				    		$("#tabella").hide();
				    		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
				    		var delay = 3000; //Your delay in milliseconds
				    		setTimeout(function(){ window.location.replace("categorie-form.jsp"); }, delay);
						});
	</script>
	<script>
	function changePage(lastPage){
		var start;
		if(lastPage != 1){
			lastPage--;
			start = lastPage*pageSize;
		}
		else{
			start = 1;
		}
		$('tbody').empty();
		$.getJSON('./webapi/categorie?start='+start+'&size='+pageSize)
		.done(function(data){
			for (var i = 0; i < data.length; i++) {
				var item = data[i];
				$("#tabella")
				.append(
						"<tr id=\""+item.nome+"\"><td><a href=\"categoria.jsp?id="+item.nome+"\">"
								+ item.nome
								+ "</a></td><td>"
								+ item.descrizione
								+ "</td><td><button onclick=\"editFormCategory("
								+ item.nome
								+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleCategory("
								+ item.nome
								+ ")\">DELETE</button></td></tr>");
    		}});
	}
	</script>
	<script src="assets/scripts/editCategory.js"></script>
	<script src="assets/scripts/deleteCategory.js"></script>
</body>
</html>