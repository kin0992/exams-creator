<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="/includes/head.jsp"></jsp:include>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/includes/menu.jsp"></jsp:include>
	<p id="intestazione"></p>
	<table id="categoria" class="tabella"></table>
	<form id="modifica-categoria"></form>
	<div class="error-message"></div>
	<script type="text/javascript">
		var nomeCategoria = "${param.id}";
		$
				.getJSON(
						'./webapi/categorie/' + nomeCategoria)
						.done(
						function(data) {
							$("#intestazione").append("Categoria " + nomeCategoria);
							$("#categoria")
									.append(
											"<th>Descrizione</th><th>Edit</th><th>Delete</th>");
							$("#categoria")
									.append(
											"<tr id=\""+nomeCategoria+"\"><td>"
													+ data.descrizione
													+ "</td><td><button onclick=\"editFormCategory("
													+ nomeCategoria
													+ ")\">EDIT</button></td><td><button onclick=\"confirmDeteleCategory("
													+ nomeCategoria
													+ ")\">DELETE</button></td></tr>");
						})
						.error(function(error){
		$("#tabella").hide();
   		$(".error-message").append("<p>Codice errore: "+error.responseJSON.codice+"<br />Messaggio: "+error.responseJSON.messaggio+"<br />Documentazione: <a href=\""+error.responseJSON.documentazione+"\">Vai alla documentazione!</a>");
   		var delay = 3000; //Your delay in milliseconds
		setTimeout(function(){ window.location.replace("categorie.jsp"); }, delay);
});
	</script>
	<script src="assets/scripts/editCategory.js"></script>
	<script src="assets/scripts/deleteCategory.js"></script>
</body>
</html>