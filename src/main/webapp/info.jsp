<html>
<head>
<title>Informazioni</title>
<jsp:include page="/includes/head.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/includes/titolo.jsp"></jsp:include>
<jsp:include page="/includes/menu.jsp"></jsp:include>
<div id="informazioni"></div>
<form id="informazioni-esame" action="webapi/info" method="POST" accept-charset=utf-8></form>
<script>
var returnTo = "${param.from}";
$.getScript("assets/scripts/deleteInfo.js");
$.getJSON('./webapi/info')
.done(function(data) {
	var item = data[0];
	var id = item.id;
	var div = $("#informazioni");
	div.append("<p>UniversitÓ: " +item.uni + "</p>");
	div.append("<p>Corso di laurea: " +item.cDL + "</p>");
	div.append("<p>Anno accademico: " +item.annoAccademico+ "</p>");
	div.append("<p>Nome corso: " +item.nomeCorso+ "</p>");
	div.append("<p>Regole esame: " +item.regoleEsame+ "</p>");
	div.append("<a href=\"info-edit.jsp\"><button>EDIT</button></a>");
	div.append("<button onclick=\"confirmDeteleInfo("+id+")\">DELETE</button>");
})
.error(function(){
	var form = $("#informazioni-esame");
	form.append("<legend>Inserire le informazioni</legend>");
	form.append("<p>UniversitÓ: <input type=\"text\" name=\"uni\" id=\"uni\" placeholder=\"Inserire nome universitÓ\" size=50 required></p>");
	form.append("<p>Corso di laurea: <input type=\"text\" name=\"corso-laurea\" id=\"corso-laurea\" placeholder=\"Inserire nome corso di laurea\" size=30 required></p>");
	form.append("<p>Anno accademico: <input type=\"text\" name=\"anno-accademico\" id=\"anno-accademico\" placeholder=\"Inserire anno accademico\" size=25 required></p>");
	form.append("<p>Nome corso: <input type=\"text\" name=\"nome-corso\" id=\"nome-corso\" placeholder=\"Inserire nome del corso\" size=35></p>");
	form.append("<p>Regole esame: <textarea name=\"regole\" rows=\"4\" cols=\"50\" placeholder=\"Inserire regole per l'esame\"></textarea></p>");
	form.append("<input type=\"submit\" id=\"submit\" value=\"Inserisci informazioni\">");
	postData();
});

</script>
<script>
function postData(){
          $("#submit").click(function(){
             var form = $(this).parents("form");
             var data = $(form).serialize();
             $.post($(form).attr("action"), data, function(event){
            	 console.log(event);
            	 if(returnTo == null || returnTo == "")
            	 	window.location.replace("info.jsp");
            	 else
            		 window.location.replace(returnTo);
             });
             return false;
           });
}
</script>
</body>
</html>
