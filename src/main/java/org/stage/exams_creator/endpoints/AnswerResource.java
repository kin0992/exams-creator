package org.stage.exams_creator.endpoints;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Answer;
import org.stage.exams_creator.models.ErrorMessage;
import org.stage.exams_creator.models.Exam;
import org.stage.exams_creator.models.Question;
import org.stage.exams_creator.services.AnswerService;
import org.stage.exams_creator.services.ExamService;
import org.stage.exams_creator.services.QuestionService;

@Path("/esami/{examId}/risposte")
@Produces(MediaType.APPLICATION_JSON)
public class AnswerResource {

	AnswerService as = new AnswerService();
	ExamService es = new ExamService();
	QuestionService qs = new QuestionService();
	CacheControl cc = new CacheControl();

	public AnswerResource() {
	}

	@GET
	public Response getAllAnswersOfExam(@PathParam("examId") String examId,
			@QueryParam("rispostaData") String rispostaData,
			@QueryParam("start") int start, @QueryParam("size") int size,
			@Context UriInfo uriInfo, @Context Request request) {
		try {
			List<Answer> risposte = new ArrayList<Answer>();
			if (rispostaData == null || rispostaData == ""
					|| rispostaData.isEmpty()) {
				risposte = as.getRisposte(examId);
			} else {
				risposte = as.getRispostaConRisposta(examId,
						rispostaData);
			}
			if (start > 0 && size > 0)
				risposte = as.getRispostePaginated(risposte, start, size);
			else{
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(new ErrorMessage("Parametri filtraggio errati", 400, "")).build());
			}
			cc.setMaxAge(0);
			EntityTag etag = new EntityTag(
					Integer.toString(risposte.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null) { // preconditions not met
				builder = Response.ok(risposte.toArray(new Answer[0]))
						.cacheControl(cc)
						.link(uriInfo.getAbsolutePath(), "self").tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 404:
				throw new WebApplicationException(
						Response.status(Status.NOT_FOUND)
								.entity(new ErrorMessage(
										"Nessuna risposta presente per questo esame.",
										404,
										"http://it.wikipedia.org/wiki/Errore_404"))
								.build());
			default:
				throw new WebApplicationException(
						Response.status(Status.INTERNAL_SERVER_ERROR)
								.entity(new ErrorMessage(
										"Qualcosa è andato storto", 500,
										"http://www.checkupdown.com/status/E500_it.html"))
								.build());
			}
		}

	}


	@GET
	@Path("/{answerId}")
	public Response getRisposta(@PathParam("examId") String examId,
			@PathParam("answerId") int answerId, @Context UriInfo uriInfo,
			@Context Request request) {
		try {
			Answer risposta = as.getRisposta(examId, answerId);
			cc.setMaxAge(0);
			cc.setNoCache(true);
			EntityTag etag = new EntityTag(
					Integer.toString(risposta.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null) {
				builder = Response.ok(risposta).cacheControl(cc)
						.link(uriInfo.getAbsolutePath(), "self").tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 404:
				throw new WebApplicationException(Response
						.status(Status.NOT_FOUND)
						.entity(new ErrorMessage("Collection not found", 404,
								"http://it.wikipedia.org/wiki/Errore_404"))
						.build());
			default:
				throw new WebApplicationException(
						Response.status(Status.INTERNAL_SERVER_ERROR)
								.entity(new ErrorMessage(
										"Qualcosa è andato storto", 500,
										"http://www.checkupdown.com/status/E500_it.html"))
								.build());
			}
		}

	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addAnswer(@PathParam("examId") String examId,
			@FormParam("domanda") int idDomanda,
			@FormParam("volte") int occorrenza,
			@FormParam("correttezza") int corret,
			@FormParam("rispostaData") String rispostaData,
			@Context UriInfo uriInfo) {
		try {
			Exam esame = es.getEsame(examId);
			double correttezza = (double) corret / 100;
			Question domanda = qs.getQuestion(idDomanda);
			double punteggio = qs.getQuestion(idDomanda).getPunteggioBase()
					* correttezza;
			Answer risposta = as.creaRisposta(occorrenza, punteggio,
					correttezza, rispostaData, esame, domanda);
			URI location = uriInfo.getAbsolutePathBuilder()
					.path(String.valueOf(risposta.getId())).build();
			return Response.created(location).build();
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 400:
				throw new WebApplicationException(
						Response.status(Status.BAD_REQUEST)
								.entity(new ErrorMessage(
										"Valore correttezza o punteggio inserito in maniera errata",
										400,
										"http://www.checkupdown.com/status/E400_it.html"))
								.build());
			default:
				throw new WebApplicationException(
						Response.status(Status.INTERNAL_SERVER_ERROR)
								.entity(new ErrorMessage(
										"Qualcosa è andato storto", 500,
										"http://www.checkupdown.com/status/E500_it.html"))
								.build());
			}
		}

	}

	@PUT
	@Path("/{answerId}")
	public Response updateAnswer(@PathParam("examId") String examId,
			@FormParam("volte") int occorrenza,
			@PathParam("answerId") int answerId,
			@FormParam("correttezza") double corret, @Context UriInfo uriInfo) {
		try {
			if (corret < 0 || corret > 100 || occorrenza < 0)
				throw new WebApplicationException(Response.status(
						Status.BAD_REQUEST).build());
			double correttezza = corret / 100;
			System.out.println(occorrenza);
			Answer updatedRisposta = as.updateRisposta(examId, answerId,
					occorrenza, correttezza);
			URI location = uriInfo.getAbsolutePathBuilder().build();
			return Response.created(location).entity(updatedRisposta).build();
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 400:
				throw new WebApplicationException(
						Response.status(Status.BAD_REQUEST)
								.entity(new ErrorMessage(
										"Valore correttezza inserito in maniera errata",
										400,
										"http://www.checkupdown.com/status/E400_it.html"))
								.build());
			default:
				throw new WebApplicationException(
						Response.status(Status.INTERNAL_SERVER_ERROR)
								.entity(new ErrorMessage(
										"Qualcosa è andato storto", 500,
										"http://www.checkupdown.com/status/E500_it.html"))
								.build());
			}
		}

	}

	@DELETE
	@Path("/{answerId}")
	public Response deleteAnswer(@PathParam("answerId") int id) {
		as.removeRisposta(id);
		return Response.status(204).build();
	}
}
