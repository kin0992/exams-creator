package org.stage.exams_creator.endpoints;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.ErrorMessage;
import org.stage.exams_creator.models.Exam;
import org.stage.exams_creator.models.Question;
import org.stage.exams_creator.services.EditableFileService;
import org.stage.exams_creator.services.ExamService;
import org.stage.exams_creator.services.PDFGeneratorService;
import org.stage.exams_creator.services.QuestionService;

import com.itextpdf.text.DocumentException;

@Path("esami")
@Produces(MediaType.APPLICATION_JSON)
public class ExamResource {

	CacheControl cc = new CacheControl();
	ExamService es = new ExamService();
	QuestionService qs = new QuestionService();

	public ExamResource() {
	}

	@GET
	public Response getAllExams(@QueryParam("anno") int anno, @QueryParam("start") int start, @QueryParam("size") int size, @Context UriInfo uriInfo, @Context Request request) {
		try{
			cc.setMaxAge(0);
			cc.setPrivate(true);
			String annoS = null;
			if (anno == 0){
				annoS = null;
			}else
				annoS = String.valueOf(anno);
			List<Exam> esami = new ArrayList<Exam>();
			esami = es.getEsami();
			if (annoS == null) {
				esami = es.getEsami();
			} else {
				esami = es.getEsamiPerAnno(anno);
			}
			if(start > 0 && size > 0){
				esami = es.getEsamiPaginated(esami, start, size);
			}
			EntityTag etag = new EntityTag(Integer.toString(esami.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null){
				builder = Response.ok(esami.toArray(new Exam[0])).cacheControl(cc)
						.link(uriInfo.getAbsolutePath(), "self")
						.link(uriInfo.getAbsolutePathBuilder()
								.path(String.valueOf(esami.get(0).getId()))
								.build(), "first")
								.link(uriInfo.getAbsolutePathBuilder().path(String.valueOf(esami.get(esami.size()-1).getId())).build(), "last").tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		}
		catch (GenericException e){
			switch (Integer.valueOf(e.getMessage())){
			case 400: throw new WebApplicationException(Response
					.status(Status.BAD_REQUEST).entity(new ErrorMessage("Bad request.", 400,
							"http://www.checkupdown.com/status/E400_it.html")).build());
			case 404: throw new WebApplicationException(Response.status(Status.NOT_FOUND)
					.entity(new ErrorMessage("Nessun esame presente",
							404, "http://it.wikipedia.org/wiki/Errore_404")).build());
			default:
				throw new WebApplicationException(Response.status(
						Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
	}

	@GET
	@Path("/{examId}")
	public Response getExamById(@PathParam("examId") String id, @Context UriInfo uriInfo, @Context Request request) {
		try {
			Exam esame = es.getEsame(id);
			cc.setMaxAge(3600);
			cc.setNoStore(true);
			cc.setPrivate(true);
			EntityTag etag = new EntityTag(Integer.toString(esame.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null){
				builder = Response.ok(esame).cacheControl(cc)
						.link(uriInfo.getAbsolutePath(), "self").tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		}
		catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())){
				case 400: throw new WebApplicationException(Response
						.status(Status.BAD_REQUEST).entity(new ErrorMessage("Bad request: combinazione data e appello errata.", 400,
								"http://www.checkupdown.com/status/E400_it.html")).build());
				case 404: throw new WebApplicationException(Response
						.status(Status.NOT_FOUND)
						.entity(new ErrorMessage("Esame " + id + " non trovato", 404,
								"http://it.wikipedia.org/wiki/Errore_404")).build());
				default:
					throw new WebApplicationException(Response.status(
							Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createExam(
			@FormParam("numeroDomanda") List<Integer> numeriDomande,
			@FormParam("puntiCambiati") List<Integer> puntiDomande,
			@FormParam("appello") int appello,
			@FormParam("data-esame") String data,
			@FormParam("logoBicocca") int logoBicocca,
			@FormParam("logoDisco") int logoDisco,
			@Context UriInfo uriInfo)
			throws IOException {
		try {
			PDFGeneratorService.HOST_NAME = uriInfo.getBaseUri().getHost();
			EditableFileService.HOST_NAME = uriInfo.getBaseUri().getHost();
			PDFGeneratorService.PORT = uriInfo.getBaseUri().getPort();
			EditableFileService.PORT = uriInfo.getBaseUri().getPort();
			PDFGeneratorService.SCHEME = uriInfo.getBaseUri().getScheme();
			EditableFileService.SCHEME = uriInfo.getBaseUri().getScheme();
			Map<Integer, Integer> domandePunti = new HashMap<>();
			for (int i = 0; i < numeriDomande.size(); i++){
				domandePunti.put(numeriDomande.get(i), puntiDomande.get(i));
			}
			List<Question> domande = new ArrayList<Question>();
			for (Integer i : numeriDomande) {
				domande.add(qs.getQuestion(i));
			}
			Exam esame = es.creaEsame(domande, domandePunti, appello, data, logoBicocca, logoDisco);
			URI location = uriInfo.getAbsolutePathBuilder().path(esame.getId())
					.build();
			Response builder = Response.created(location).build();
			return builder;
		}
		catch(GenericException e) {
			switch (Integer.valueOf(e.getMessage())){
			case 400: throw new WebApplicationException(Response.status(
					Status.BAD_REQUEST).entity(new ErrorMessage("Bad request: combinazione data e appello errata o dati mancanti.", 400, "http://www.checkupdown.com/status/E400_it.html")).build());
			case 409: throw new WebApplicationException(Response.status(Status.CONFLICT).entity(new ErrorMessage("Conflict error: controllare data esame e appello.", 409, "http://www.checkupdown.com/status/E409.html"))
					.build());
			default:
				throw new WebApplicationException(Response.status(
						Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
		
	}
	
	@PUT
	@Path("/{examId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateExam(
			@PathParam("examId") String examId,
			@FormParam("numeroDomanda") List<Integer> numeriDomande,
			@FormParam("puntiCambiati") List<Integer> puntiDomande,
			@FormParam("appello") int appello,
			@FormParam("data-esame") String data,
			@FormParam("studenti_presenti") int presenti,
			@FormParam("congela") boolean congelato,
			@FormParam("crea") boolean creato,
			@FormParam("scongela") boolean sbloccato,
			@FormParam("logoBicocca") int logoBicocca,
			@FormParam("logoDisco") int logoDisco,
			@Context UriInfo uriInfo,
			@Context Request request)
			throws IOException, GenericException, DocumentException {
		try {
			if (creato || congelato || sbloccato){
				logoBicocca = es.getEsame(examId).getLogoUni();
				logoDisco = es.getEsame(examId).getLogoDisco();
			}
			PDFGeneratorService.HOST_NAME = uriInfo.getBaseUri().getHost();
			PDFGeneratorService.PORT = uriInfo.getBaseUri().getPort();
			PDFGeneratorService.SCHEME = uriInfo.getBaseUri().getScheme();
			List<Question> domande = new ArrayList<Question>();
			for (Integer i : numeriDomande) {
				domande.add(qs.getQuestion(i));
			}
			Map<Integer, Integer> domandePunti = new HashMap<>();
			for (int i = 0; i < numeriDomande.size(); i++){
				domandePunti.put(numeriDomande.get(i), puntiDomande.get(i));
			}
			Exam esame = es.updateExam(examId, domande, appello, data, domandePunti, logoBicocca, logoDisco);
			if(congelato == true)
				es.bloccaEsame(examId);
			if(sbloccato == true)
				es.sbloccaEsame(examId);
			if(creato == true) 
				es.creaPdf(examId);
			if(presenti > 0)
				es.aggiornaPresenti(examId, presenti);
			URI location = uriInfo.getAbsolutePathBuilder().build();
			Response builder = Response.created(location).entity(esame).build();
			return builder;
		}
		catch(GenericException e) {
			switch (Integer.valueOf(e.getMessage())){
			case 400: throw new WebApplicationException(Response.status(
					Status.BAD_REQUEST).entity(new ErrorMessage("Bad request", 400, "http://www.checkupdown.com/status/E400_it.html")).build());
			case 409: throw new WebApplicationException(Response.status(Status.CONFLICT).entity(new ErrorMessage("Esiste già un esame con appello "+ appello + "!", 409, "http://www.checkupdown.com/status/E409.html")).build());
			default:
				e.getMessage();
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@DELETE
	@Path("/{examId}")
	public Response deleteExam(@PathParam("examId") String id) {
		try {
			es.removeEsame(id);
			return Response.status(204).build();
		}
		catch (GenericException e){
			throw new WebApplicationException(Response.status(
					Status.NOT_FOUND).entity(new ErrorMessage("Not Found", 404, "http://it.wikipedia.org/wiki/Errore_404")).build());
	}

	}
}