package org.stage.exams_creator.endpoints;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Category;
import org.stage.exams_creator.models.ErrorMessage;
import org.stage.exams_creator.models.Question;
import org.stage.exams_creator.services.CategoryService;
import org.stage.exams_creator.services.QuestionService;

@Path("domande")
@Produces(MediaType.APPLICATION_JSON)
public class QuestionResource {

	QuestionService questionService = new QuestionService();
	CategoryService categoryService = new CategoryService();
	CacheControl cc = new CacheControl();
	public static String IMAGE_LOCATION="";

	public QuestionResource() {
	}

	@GET
	public Response getAllQuestions(@QueryParam("categoria") String categoria, @QueryParam("order") String order, @QueryParam ("type") String type, @QueryParam("start") int start, @QueryParam("size") int size,
			@Context UriInfo uriInfo, @Context Request request) {
		try {
			List<Question> domande = new ArrayList<Question>();
			cc.setMaxAge(0);
			if (categoria == null) {
				domande = questionService.getDomande();
			} else {
				domande = questionService.getDomandePerCategoria(categoria);
			}
			if(type == null) {
			}else if (type.equalsIgnoreCase("aperte")){
				domande = questionService.getDomandeAperte(domande);
			}
			else if(type.equalsIgnoreCase("chiuse")){
				domande = questionService.getDomandeChiuse(domande);
			}else
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(new ErrorMessage("Parametro di filtraggio errato", 400, "")).build());
			if (domande.isEmpty())
				throw new WebApplicationException(
						Response.status(Status.NOT_FOUND).entity(new ErrorMessage("Nessuna domanda presente nel DB",
								404, "http://it.wikipedia.org/wiki/Errore_404")).build());
			if (order == null) {
			} else if (order.equalsIgnoreCase("count")) {
				questionService.orderQuestionByCont(domande);
			} else if (order.equalsIgnoreCase("appello")) {
				questionService.orderQuestionByLastAppello(domande);
			}else
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(new ErrorMessage("Parametro di filtraggio errato", 400, "")).build());
			if(start > 0 && size > 0){
				domande = questionService.getDomandePaginated(domande, start, size);
			}
			EntityTag etag = new EntityTag(Integer.toString(domande.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null) {
				builder = Response.ok(domande.toArray(new Question[0])).link(uriInfo.getAbsolutePath(), "self")
						.link(uriInfo.getAbsolutePathBuilder().path(String.valueOf(domande.get(0).getNumero())).build(),
								"first")
						.link(uriInfo.getAbsolutePathBuilder().path(String.valueOf(domande.size())).build(), "last")
						.cacheControl(cc).tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 404:
				throw new WebApplicationException(
						Response.status(Status.NOT_FOUND).entity(new ErrorMessage("Nessuna domanda presente nel DB",
								404, "http://it.wikipedia.org/wiki/Errore_404")).build());

			default:
				throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ErrorMessage("Qualcosa è andato storto", 500,
								"http://www.checkupdown.com/status/E500_it.html"))
						.build());
			}
		}
	}

	@GET
	@Path("/{numeroDomanda}")
	public Response getQuestion(@PathParam("numeroDomanda") int numeroDomanda, @Context UriInfo uriInfo,
			@Context Request request) throws IOException {
		try {
			Question domanda = questionService.getQuestion(numeroDomanda);
			cc.setMaxAge(0);
			EntityTag etag = new EntityTag(Integer.toString(domanda.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null) {
				builder = Response.ok(domanda).cacheControl(cc).link(uriInfo.getAbsolutePath(), "self").tag(etag);
				// .link(uriInfo.getAbsolutePath(), "next")
				// .link(uriInfo.getBaseUri(), "previous")
			}
			builder.cacheControl(cc);
			return builder.build();
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 404:
				throw new WebApplicationException(Response.status(Status.NOT_FOUND)
						.entity(new ErrorMessage("Domanda " + numeroDomanda + " non trovata", 404,
								"http://it.wikipedia.org/wiki/Errore_404"))
						.build());
			default:
				throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ErrorMessage("Qualcosa è andato storto", 500,
								"http://www.checkupdown.com/status/E500_it.html"))
						.build());
			}
		}

	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addQuestion(@FormDataParam("testo") String testo,
								@FormDataParam("risposta") List<String> risposta,
								@FormDataParam("categorie") List<String> categ,
								@FormDataParam("immagine") InputStream image,
								@FormDataParam("immagine") FormDataContentDisposition details,
								@FormDataParam("spazi-bianchi") int spaziBianchi,
								@FormDataParam("opzioni-risposta") List<String> opzioni,
								@FormDataParam("punteggio-base") int punteggioBase,
								@Context UriInfo uriInfo) throws IOException {
		try{
			String imageLocation = "";
			String imageName = "";
			if(image == null || details.getFileName().isEmpty()){
			}
			else{
				imageLocation = IMAGE_LOCATION + "/" +details.getFileName();
				File f = new File(imageLocation);
				imageName=details.getFileName();
				OutputStream outStream = new FileOutputStream(f);
				byte[] buffer = new byte[8 * 1024];
				int bytesRead;
				while ((bytesRead = image.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}
				outStream.close();
			}
			List<Category> categorie = new ArrayList<Category>();
			for(String categoria: categ){
				categorie.add(categoryService.getCategoria(categoria));
			}
			Question domanda = questionService.addQuestion(testo, risposta, categorie, imageName, opzioni, punteggioBase, spaziBianchi);
			String newId = String.valueOf(domanda.getNumero());
			URI location = uriInfo.getAbsolutePathBuilder().path(newId).build();
			Response response = Response.created(location).build();
			return response;
		}catch (GenericException e){
			switch (Integer.valueOf(e.getMessage())) {
				case 400: throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(new ErrorMessage("Bad Request", 400, "http://www.checkupdown.com/status/E400_it.html")).build());
				default: throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR).build());
			}
		}
	}

	@PUT
	@Path("/{numeroDomanda}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateQuestion(@PathParam("numeroDomanda") int numeroDomanda, @FormDataParam("testo") String testo,
			@FormDataParam("risposta") List<String> risposta, @FormDataParam("categorie") List<String> categ,
			@FormDataParam("immagine") InputStream image,
			@FormDataParam("immagine") FormDataContentDisposition details,
			@FormDataParam("spazi-bianchi") int spaziBianchi,
			@FormDataParam("opzioni-risposta") List<String> opzioni,
			@FormDataParam("punteggio-base") int punteggioBase, @Context UriInfo uriInfo) throws GenericException, IOException {
		try {
			String imagePath;
			String imageLocation = "";
			if(image == null || details.getFileName().isEmpty()){
				imagePath="";
			}
			else{
				imagePath="assets/images/domande/"+details.getFileName();
				imageLocation = IMAGE_LOCATION+"/"+details.getFileName();
				File f = new File(imageLocation);
				OutputStream outStream = new FileOutputStream(f);
				byte[] buffer = new byte[8 * 1024];
				int bytesRead;
				while ((bytesRead = image.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}
				outStream.close();
			}
			List<Category> categorie = new ArrayList<Category>();
			for (String categoriaForm : categ) {
				categorie.add(categoryService.getCategoria(categoriaForm));
			}
			
			Question updatedDomanda = questionService.updateQuestion(numeroDomanda, testo, risposta, categorie,
					imagePath, opzioni, punteggioBase, spaziBianchi);
			URI location = uriInfo.getAbsolutePathBuilder().build();
			Response response = Response.created(location).entity(updatedDomanda).build();
			return response;
		} catch (GenericException e) {
			switch (Integer.valueOf(e.getMessage())) {
			case 400:
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
						.entity(new ErrorMessage("Bad request", 400, "http://www.checkupdown.com/status/E400_it.html"))
						.build());
			default:
				throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new ErrorMessage("Qualcosa è andato storto", 500,
								"http://www.checkupdown.com/status/E500_it.html"))
						.build());
			}
		}
	}

	@DELETE
	@Path("/{numeroDomanda}")
	public Response deleteQuestion(@PathParam("numeroDomanda") int numeroDomanda) {
		try {
			questionService.removeQuestion(numeroDomanda);
			return Response.status(204).build();
		} catch (GenericException e) {
			return null;
		}
	}
}
