package org.stage.exams_creator.endpoints;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.ErrorMessage;
import org.stage.exams_creator.models.Info;
import org.stage.exams_creator.services.InfoService;

@Path("info")
@Produces(MediaType.APPLICATION_JSON)
public class InfoResource {
	
	InfoService is = new InfoService();
	CacheControl cc = new CacheControl();

	public InfoResource() {
	}
	
	@GET
	public Response getInfos(@Context UriInfo uriInfo, @Context Request request){
		try{
			List<Info> informazioni = is.getInformazioniEsami();	
			cc.setMaxAge(0);
			EntityTag etag = new EntityTag(Integer.toString(informazioni.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null){
				builder = Response.ok(informazioni.toArray(new Info[0]))
						.cacheControl(cc)
						.link(uriInfo.getAbsolutePath(), "self").tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		}
		catch(GenericException e){
			switch(Integer.valueOf(e.getMessage())){
			
			
		case 404:throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(new ErrorMessage("Nessuna informazione presente nel DB", Integer.valueOf(e.getMessage()), "http://it.wikipedia.org/wiki/Errore_404")).build());
		default:
			throw new WebApplicationException(Response.status(
					Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
	}
	
	@GET
	@Path("{infoId}")
	public Response getInformation(@PathParam("infoId") int id, @Context UriInfo uriInfo, @Context Request request){
		try{
			Info info = is.getInformazioniEsame(id);
			cc.setMaxAge(3600);
			EntityTag etag = new EntityTag(Integer.toString(info.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null){
				builder = Response.ok(info)
						.cacheControl(cc)
						.link(uriInfo.getAbsolutePath(), "self").tag(etag);
			}
			builder.cacheControl(cc);
			return builder.build();
		}
		catch(GenericException e){
			switch(Integer.valueOf(e.getMessage())){
			
			
		case 404:throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(new ErrorMessage("Nessuna informazione presente nel DB", Integer.valueOf(e.getMessage()), "http://it.wikipedia.org/wiki/Errore_404")).build());
		default:
			throw new WebApplicationException(Response.status(
					Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response saveInfo(@FormParam("uni") String uni, @FormParam("corso-laurea") String cDL, @FormParam("anno-accademico") String annoAccademico, @FormParam("nome-corso")String nomeCorso, @FormParam("regole") String regoleEsame, @Context UriInfo uriInfo){
		Info informazioni = is.addInfo(uni, cDL, annoAccademico, nomeCorso, regoleEsame);
		URI location = uriInfo.getAbsolutePath();
		Response builder = null;
		builder = Response.status(Status.CREATED).entity(informazioni).location(location).build();
		return builder;
	}
	
	@PUT
	@Path("{infoId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateInfo(@PathParam("infoId") int infoId, @FormParam("uni") String uni, @FormParam("corso-laurea") String cDL, @FormParam("anno-accademico") String annoAccademico, @FormParam("nome-corso")String nomeCorso, @FormParam("regole") String regoleEsame, @Context UriInfo uriInfo){
		try{
			Info newInfo = is.updateInformazioniEsami(infoId, uni, cDL, annoAccademico, nomeCorso, regoleEsame);
			URI location = uriInfo.getAbsolutePath();
			Response builder = Response.status(Status.OK).entity(newInfo).location(location).build();
			return builder;
		}
		catch(GenericException e){
			switch(Integer.valueOf(e.getMessage())){
			case 404: throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(new ErrorMessage("Nessuna informazione presente nel DB", Integer.valueOf(e.getMessage()), "http://it.wikipedia.org/wiki/Errore_404")).build());
			default:
				throw new WebApplicationException(Response.status(
						Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
	}
	
	@DELETE
	@Path("{infoId}")
	public Response removeInfo(@PathParam("infoId") int id){
		is.deleteInfo(id);
		return Response.status(204).build();
	}
}
