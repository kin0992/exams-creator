package org.stage.exams_creator.endpoints;


import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Category;
import org.stage.exams_creator.models.ErrorMessage;
import org.stage.exams_creator.services.CategoryService;

@Path("categorie")
@Produces(MediaType.APPLICATION_JSON)
public class CategoryResource {
	
	CategoryService categoryService = new CategoryService();
	CacheControl cc = new CacheControl();

	public CategoryResource() {
	}
	
	@GET
	public Response getAllCategories(@Context UriInfo uriInfo, @Context Request request, @QueryParam("start") int start, @QueryParam("size") int size) throws IOException {
		try{
			List<Category> lista = new ArrayList<Category>();
			if(start > 0 && size > 0){
				lista = categoryService.getCategoriePaginated(start, size);
			}else{
				lista = categoryService.getCategorie();
			}
			cc.setMaxAge(3600);
			EntityTag etag = new EntityTag(Integer.toString(lista.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			if (builder == null){
				builder = Response.ok(lista.toArray(new Category[0])).tag(etag)
						.link(uriInfo.getAbsolutePath(), "self").cacheControl(cc);
			}
			builder.cacheControl(cc);
			return builder.build();
		}
		catch (GenericException e){
			switch (Integer.valueOf(e.getMessage())){
			case 404: throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(new ErrorMessage("Nessuna categoria presente nel DB", Integer.valueOf(e.getMessage()), "http://it.wikipedia.org/wiki/Errore_404")).build());
			default : throw new WebApplicationException(Response.status(
					Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
		
	}
	
	@GET
	@Path("/{nomeCategoria}")
	public Response getCategory(@PathParam("nomeCategoria") String nome, @Context UriInfo uriInfo, @Context Request request){
		try{
			Category categoria = categoryService.getCategoria(nome);
			EntityTag etag = new EntityTag(Integer.toString(categoria.hashCode()));
			ResponseBuilder builder = request.evaluatePreconditions(etag);
			cc.setMaxAge(3600);
			if (builder == null){
				builder = Response.ok(categoria).tag(etag).link(uriInfo.getAbsolutePath(), "self");
			}
			builder.cacheControl(cc);
			return builder.build();
		}
		catch (GenericException e){
			switch(Integer.valueOf(e.getMessage())){
			case 404 :throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(new ErrorMessage ("Categoria "+ nome +" non trovata", Integer.valueOf(e.getMessage()), "http://it.wikipedia.org/wiki/Errore_404")).build());
				default: throw new WebApplicationException(Response.status(
						Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
		
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addCategory(@FormParam("nome") String nome,
								@FormParam("descrizione") String descrizione,
								@Context UriInfo uriInfo) throws IOException{
		try{
			Category categoria = categoryService.addCategoria(nome, descrizione);
			URI location = uriInfo.getAbsolutePathBuilder()
					.path(categoria.getNome())
					.build();
			Response builder = Response.created(location)
					.build();
			return builder;
		}
		catch (GenericException e){
			switch(Integer.valueOf(e.getMessage())){
			case 400: throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(new ErrorMessage("Nome categoria mancante", Integer.valueOf(e.getMessage()), "http://www.checkupdown.com/status/E400_it.html")).build());
			case 409: throw new WebApplicationException(Response.status(Status.CONFLICT).entity(new ErrorMessage("Categoria " + nome + " già presente nel database", Integer.valueOf(e.getMessage()), "http://www.checkupdown.com/status/E409.html")).build());
				default: throw new WebApplicationException(Response.status(
						Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
		
	}
	
	@PUT
	@Path("/{nomeCategoria}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateCategory(@PathParam("nomeCategoria") String nome,
									@FormParam("nome") String newNome,
									@FormParam("descrizione") String newDescrizione,
									@Context UriInfo uriInfo) throws IOException, GenericException{
		try{
			Category updatedCategoria = categoryService.updateCategoria(nome, newNome, newDescrizione);
			URI location = uriInfo.getAbsolutePathBuilder()
					.build();
			Response builder = Response.ok(location)
					.entity(updatedCategoria)
					.build();
			return builder;
		}
		catch (GenericException e){
			switch(Integer.valueOf(e.getMessage())){
			case 409: throw new WebApplicationException(Response.status(Status.CONFLICT).entity(new ErrorMessage("Categoria " + newNome + " già presente nel database", Integer.valueOf(e.getMessage()), "http://www.checkupdown.com/status/E409.html")).build());
				default: throw new WebApplicationException(Response.status(
						Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage("Qualcosa è andato storto", 500, "http://www.checkupdown.com/status/E500_it.html")).build());
			}
		}
		
	}

	@DELETE
	@Path("/{nomeCategoria}")
	public Response deleteCategory(@PathParam("nomeCategoria") String nome) {
		try {
			categoryService.removeCategoria(nome);
			return Response.status(204).build();
		}
		catch(GenericException e){
			return null;
		}
		
	}
}
