package org.stage.exams_creator;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.stage.exams_creator.endpoints.AnswerResource;
import org.stage.exams_creator.endpoints.CategoryResource;
import org.stage.exams_creator.endpoints.ExamResource;
import org.stage.exams_creator.endpoints.InfoResource;
import org.stage.exams_creator.endpoints.QuestionResource;

public class MyApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<Class<?>>();
		// register resources and features
		classes.add(MultiPartFeature.class);
		classes.add(QuestionResource.class);
		classes.add(LoggingFilter.class);
		classes.add(CategoryResource.class);
		classes.add(ExamResource.class);
		classes.add(AnswerResource.class);
		classes.add(InfoResource.class);
		return classes;
	}
}
