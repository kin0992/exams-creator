package org.stage.exams_creator.models;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

@XmlRootElement
@XmlType(propOrder={"id", "occorrenze", "punteggio", "correttezza", "rispostaData", "esame", "domanda"})
@Entity(value = "risposte", noClassnameStored=true)
public class Answer {
	
	@Id int id;
	private int occorrenze;
	private double punteggio; //punti ricevuti per la risposta
	private double correttezza;
	private String rispostaData;//correttezza della risposta [0,1]
	@Reference ("esame") Exam esame;
	@Reference ("domanda") Question domanda;
	private boolean cancellato;

	public Answer() {
	}
	
	public Answer(int id, int nVolte, double punteggio, double correttezza, String rispostaData, Exam esame,
			Question domanda) {
		this.id = id;
		this.occorrenze = nVolte;
		this.punteggio = punteggio;
		this.correttezza = correttezza;
		this.rispostaData = rispostaData;
		this.esame = esame;
		this.domanda = domanda;
		this.cancellato = false;
	}

	public Answer(int occorrenze, double punteggio, double correttezza, String rispostaData, Exam esame,
			Question domanda) {
		this.punteggio = punteggio;
		this.correttezza = correttezza;
		this.occorrenze = occorrenze;
		this.rispostaData = rispostaData;
		this.esame = esame;
		this.domanda = domanda;
		this.cancellato = false;
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}
	
	public int getOccorrenze() {
		return occorrenze;
	}

	public void setOccorrenze(int occorrenze) {
		this.occorrenze = occorrenze;
	}

	public double getPunteggio() {
		return punteggio;
	}

	public void setPunteggio(double punteggio) {
		this.punteggio = punteggio;
	}

	public double getCorrettezza() {
		return correttezza;
	}

	public void setCorrettezza(double correttezza) {
		this.correttezza = correttezza;
	}
	public Exam getEsame(){
		return esame;
	}
	public void setEsame(Exam esame){
		this.esame = esame;
	}
	public Question getDomanda(){
		return domanda;
	}
	public void setDomanda(Question domanda){
		this.domanda = domanda;
	}
	@XmlTransient
	public boolean isCancellato() {
		return cancellato;
	}

	public void setCancellato(boolean cancellato) {
		this.cancellato = cancellato;
	}

	public String getRispostaData() {
		return rispostaData;
	}

	public void setRispostaData(String rispostaData) {
		this.rispostaData = rispostaData;
	}
}
