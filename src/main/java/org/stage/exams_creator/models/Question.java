package org.stage.exams_creator.models;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

@XmlRootElement
@XmlType(propOrder={"numero", "testoDomanda", "immagineDomanda", "opzioniRisposta", "rispostaDomanda", "punteggioBase", "categorie", "appelli", "contatore", "aperta", "spaziBianchi"})
@Entity(value="domande",noClassnameStored=true)
public class Question {
	
	@Id private int numero;
	private String testoDomanda;
	private List<String> rispostaDomanda;
	@Reference ("categorie") private List<Category> categorie = new ArrayList<Category>();
	private String immagineDomanda;
	private List<String> appelli;
	private int contatore;
	private List<String> opzioniRisposta;
	private int punteggioBase;
	private boolean cancellato;
	private boolean aperta;
	private int spaziBianchi;

	public Question() {
	}

	public Question(int numero, String testoDomanda, List<String> rispostaDomanda,
			List<Category> categorie, String immagineDomanda,
			List<String> opzioniRisposta, int punteggioBase, int spaziBianchi) {
		this.numero = numero;
		this.testoDomanda = testoDomanda;
		this.rispostaDomanda = rispostaDomanda;
		this.categorie = categorie;
		this.immagineDomanda = immagineDomanda;
		this.appelli = new ArrayList<String>();
		this.appelli.add("");
		this.contatore = 0;
		if(opzioniRisposta.isEmpty())
			this.opzioniRisposta = null;
		else
			this.opzioniRisposta = opzioniRisposta;
		this.punteggioBase = punteggioBase;
		this.cancellato = false;
		if (opzioniRisposta.isEmpty()){
			this.aperta = true;
			this.spaziBianchi = spaziBianchi;
		}
		else{
			this.aperta = false;
			this.spaziBianchi = 0;
		}
	}
	public Question(String testoDomanda, List<String> rispostaDomanda,
			List<Category> categorie, String immagineDomanda,
			List<String> opzioniRisposta, int punteggioBase, int spaziBianchi) {
		this.testoDomanda = testoDomanda;
		this.rispostaDomanda = rispostaDomanda;
		this.categorie = categorie;
		this.immagineDomanda = immagineDomanda;
		this.appelli = new ArrayList<String>();
		this.appelli.add("");
		this.contatore = 0;
		this.opzioniRisposta = opzioniRisposta;
		this.punteggioBase = punteggioBase;
		this.cancellato = false;
		if (opzioniRisposta.isEmpty()){
			this.aperta = true;
			this.spaziBianchi = spaziBianchi;
		}
		else{
			this.aperta = false;
			this.spaziBianchi = spaziBianchi;
		}
	}
	

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTestoDomanda() {
		return testoDomanda;
	}

	public void setTestoDomanda(String testoDomanda) {
		this.testoDomanda = testoDomanda;
	}

	public List<String> getRispostaDomanda() {
		return rispostaDomanda;
	}

	public void setRispostaDomanda(List<String> rispostaDomanda) {
		this.rispostaDomanda = rispostaDomanda;
	}

	public List<Category> getCategorie() {
		return categorie;
	}

	public void setCategorie(List<Category> categorie) {
		this.categorie = categorie;
	}

	public String getImmagineDomanda() {
		return immagineDomanda;
	}

	public void setImmagineDomanda(String immagineDomanda) {
		this.immagineDomanda = immagineDomanda;
	}

	public List<String> getAppelli() {
		return appelli;
	}

	public void setAppelli(List<String> appelli) {
			this.appelli = appelli;
	}

	public int getContatore() {
		return contatore;
	}

	public void setContatore(int contatore) {
		this.contatore = contatore;
	}

	public List<String> getOpzioniRisposta() {
		return opzioniRisposta;
	}

	public void setOpzioniRisposta(List<String> opzioniRisposta) {
		this.opzioniRisposta = opzioniRisposta;
	}

	public int getPunteggioBase() {
		return punteggioBase;
	}

	public void setPunteggioBase(int punteggioBase) {
		this.punteggioBase = punteggioBase;
	}
	@XmlTransient
	public boolean isCancellato() {
		return cancellato;
	}

	public void setCancellato(boolean cancellato) {
		this.cancellato = cancellato;
	}

	public boolean isAperta() {
		return aperta;
	}

	public void setAperta(boolean aperta) {
		this.aperta = aperta;
	}
	public int getSpaziBianchi() {
		return spaziBianchi;
	}

	public void setSpaziBianchi(int spaziBianchi) {
		this.spaziBianchi = spaziBianchi;
	}
	
}