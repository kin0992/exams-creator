package org.stage.exams_creator.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

@XmlRootElement
@XmlType(propOrder={"id", "data", "appelloNumero", "domande", "studentiPresenti", "fileName", "congelato", "creato", "puntiDomande", "logoUni", "logoDisco"})
@Entity(value="esami", noClassnameStored=true)
public class Exam {
	
	
	@Id private String id; //formato aaaa0k, con k numero appello
	private String data;
	@Reference ("domande")private List<Question> domande = new ArrayList<Question>();
	private Map<Integer, Integer> puntiDomande = new HashMap<>();
	private int appelloNumero;
	private int studentiPresenti;
	private String fileName;
	private boolean cancellato;
	private boolean congelato;
	private boolean creato;
	private int logoUni;
	private int logoDisco;

	public Exam() {
	}
	
	public Exam(String id, String data, List<Question> domande,
			int appelloNumero, String filePath, Map<Integer, Integer> puntiDomande, int logoUni, int logoDisco) {
		this.id = id;
		this.data = data;
		this.domande = domande;
		this.puntiDomande = puntiDomande;
		this.appelloNumero = appelloNumero;
		this.fileName = filePath;
		this.studentiPresenti = 0;
		this.cancellato = false;
		this.congelato = false;
		this.creato = false;
		this.logoUni = logoUni;
		this.logoDisco = logoDisco;
	}
	
	public int getAppelloNumero() {
		return appelloNumero;
	}
	public void setAppelloNumero(int appelloNumero) {
		this.appelloNumero = appelloNumero;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getStudentiPresenti() {
		return studentiPresenti;
	}
	public void setStudentiPresenti(int studentiPresenti) {
		this.studentiPresenti = studentiPresenti;
	}
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return id;
	}
	public List<Question> getDomande(){
		return domande;
	}
	public void setDomande(List<Question> domande) {
		this.domande = domande;
	}
	public Map<Integer, Integer> getPuntiDomande() {
		return puntiDomande;
	}

	public void setPuntiDomande(Map<Integer, Integer> puntiDomande) {
		this.puntiDomande = puntiDomande;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@XmlTransient
	public boolean isCancellato() {
		return cancellato;
	}

	public void setCancellato(boolean cancellato) {
		this.cancellato = cancellato;
	}

	public boolean isCongelato() {
		return congelato;
	}

	public void setCongelato(boolean congelato) {
		this.congelato = congelato;
	}

	public boolean isCreato() {
		return creato;
	}

	public void setCreato(boolean creato) {
		this.creato = creato;
	}
	public int getLogoUni() {
		return logoUni;
	}

	public void setLogoUni(int logoUni) {
		this.logoUni = logoUni;
	}
	public int getLogoDisco() {
		return logoDisco;
	}

	public void setLogoDisco(int logoDisco) {
		this.logoDisco = logoDisco;
	}
}