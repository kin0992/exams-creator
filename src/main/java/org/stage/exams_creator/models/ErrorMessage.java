package org.stage.exams_creator.models;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class ErrorMessage {
	
	private String messaggio;
	private int codice;
	private String documentazione;

	public ErrorMessage() {
	}

	public ErrorMessage(String messaggio, int codice, String documentazione) {
		this.messaggio = messaggio;
		this.codice = codice;
		this.documentazione = documentazione;
	}

	public String getMessaggio() {
		return messaggio;
	}

	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}

	public int getCodice() {
		return codice;
	}

	public void setCodice(int codice) {
		this.codice = codice;
	}

	public String getDocumentazione() {
		return documentazione;
	}

	public void setDocumentazione(String documentazione) {
		this.documentazione = documentazione;
	}

}
