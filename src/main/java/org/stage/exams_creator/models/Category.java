package org.stage.exams_creator.models;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@XmlRootElement
@XmlType(propOrder={"id", "nome", "descrizione"})
@Entity(value="categorie", noClassnameStored=true)
public class Category {
	
	@Id int id;
	private String nome;
	private String descrizione;
	private boolean cancellato;

	public Category() {
	}

	public Category(String nome, String descrizione) {
		this.nome = nome;
		this.descrizione = descrizione;
		this.cancellato = false;
	}
	
	public Category(int id, String nome, String descrizione) {
		this.id = id;
		this.nome = nome;
		this.descrizione = descrizione;
		this.cancellato = false;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	@XmlTransient
	public boolean isCancellato() {
		return cancellato;
	}

	public void setCancellato(boolean cancellato) {
		this.cancellato = cancellato;
	}
}
