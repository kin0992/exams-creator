package org.stage.exams_creator.models;

import javax.xml.bind.annotation.XmlRootElement;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;


@XmlRootElement
@Entity(value="info", noClassnameStored=true)
public class Info {
	
	@Id private int id;
	private String uni;
	private String cDL;
	private String annoAccademico;
	private String nomeCorso;
	private String regoleEsame;
	
	public Info() {
	}
	public Info(int id, String uni, String cDL, String annoAccademico,
			String nomeCorso, String regoleEsame) {
		super();
		this.id = id;
		this.uni = uni;
		this.cDL = cDL;
		this.annoAccademico = annoAccademico;
		this.nomeCorso = nomeCorso;
		this.regoleEsame = regoleEsame;
	}
	public Info(String uni,String cDL, String annoAccademico, String nomeCorso,
			String regoleEsame) {
		this.uni = uni;
		this.cDL = cDL;
		this.annoAccademico = annoAccademico;
		this.nomeCorso = nomeCorso;
		this.regoleEsame = regoleEsame;
	}
	public String getUni() {
		return uni;
	}
	public void setUni(String uni) {
		this.uni = uni;
	}
	public String getcDL() {
		return cDL;
	}
	public void setcDL(String cDL) {
		this.cDL = cDL;
	}
	public String getAnnoAccademico() {
		return annoAccademico;
	}
	public void setAnnoAccademico(String annoAccademico) {
		this.annoAccademico = annoAccademico;
	}
	public String getNomeCorso() {
		return nomeCorso;
	}
	public void setNomeCorso(String nomeCorso) {
		this.nomeCorso = nomeCorso;
	}
	public String getRegoleEsame() {
		return regoleEsame;
	}
	public void setRegoleEsame(String regoleEsame) {
		this.regoleEsame = regoleEsame;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
}
