package org.stage.exams_creator.services;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public abstract class BaseService {


	private static final String SERVER = "localhost";
	private static final String NOME_DB = "sistemiDistribuiti";
	private static final int PORT = 27017;
	private static Datastore datastore;
	
	public BaseService() {
		if(datastore == null) {
			Morphia morphia =  new Morphia();
			morphia.mapPackage("org.stage.exams_creator.models");
			datastore = morphia.createDatastore(new MongoClient(SERVER, PORT), NOME_DB);
		}
	}
	
	protected Datastore getDatastore() {
		return datastore;
	}
}
