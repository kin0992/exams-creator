package org.stage.exams_creator.services;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Exam;
import org.stage.exams_creator.models.Question;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

public class PDFGeneratorService {
	
	QuestionService qs = new QuestionService();
	InfoService is = new InfoService();
	public static String FILE_LOCATION ="";
	public static String HOST_NAME = "";
	public static String SCHEME = "";
	public static int PORT;

	public PDFGeneratorService() {
	}
	
	public void creaFileEsame(Exam esame) throws GenericException, MalformedURLException, IOException{
			List<Question> domande = esame.getDomande();
			int numDomande = domande.size();
			Map<Integer, Integer> puntiDom = esame.getPuntiDomande();
			for (Question domanda : domande){
				domanda.setPunteggioBase(puntiDom.get(domanda.getNumero()));
			}
			List<Question> open = new ArrayList<Question>();
			List<Question> close = new ArrayList<Question>();
			for (Question domanda : domande){
				if(domanda.isAperta())
					open.add(domanda);
				else
					close.add(domanda);
			}
			String university = is.getInformazioniEsami().get(0).getUni();
			String cDL = is.getInformazioniEsami().get(0).getcDL();
			String aA = is.getInformazioniEsami().get(0).getAnnoAccademico();
			String nomeCorso = is.getInformazioniEsami().get(0).getNomeCorso();
			Document document = new Document(PageSize.A4);
			try {
				String nameExam = FILE_LOCATION + "/"+esame.getFileName() + ".pdf";
				PdfWriter.getInstance(document, new FileOutputStream(nameExam));
				document.open();
				Paragraph uni = new Paragraph(university, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 15));
				Paragraph corsoLaurea = new Paragraph("LAUREA IN "+cDL.toUpperCase(), FontFactory.getFont(FontFactory.HELVETICA, 15));
				Paragraph annoAccademico = new Paragraph("ANNO ACCADEMICO: "+aA, FontFactory.getFont(FontFactory.HELVETICA, 12));
				Paragraph corso = new Paragraph(nomeCorso, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 15));
				Paragraph date = new Paragraph(esame.getData(), FontFactory.getFont(FontFactory.HELVETICA, 10));
				PdfPTable ptiTable = new PdfPTable(numDomande + 1);
				PdfPCell qNumberCell;
				for(int i = 0; i < numDomande; i++){
					scriviNumeroDomanda(ptiTable, i);
				}
				PdfPCell qTotCell = new PdfPCell(new Paragraph("TOT"));
				qTotCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				ptiTable.addCell(qTotCell);
				int totale = 0;
				int numero = 1;
				if(esame.getLogoUni() == 1){
					Image imageLogo = Image.getInstance(new URL(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo.jpg"));
					imageLogo.setAbsolutePosition(30f, 700f);
					imageLogo.scaleAbsolute(100f, 100f);
					document.add(imageLogo);
				}
				if(esame.getLogoDisco() == 1){
					Image discoLogo = Image.getInstance(new URL(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo_disco.png"));
					discoLogo.setAbsolutePosition(450f, 700f);
					discoLogo.scaleAbsolute(100f, 100f);
					document.add(discoLogo);
				}
				List<PdfPTable> orderedList = new ArrayList<PdfPTable>();
				for(Question domanda : close){
					PdfPTable tabellaDomanda = new PdfPTable(1);
					tabellaDomanda.setWidthPercentage(100);
					PdfPCell cellaTesto = new PdfPCell();
					cellaTesto.setBorder(PdfPCell.NO_BORDER);
					Paragraph testo = new Paragraph(String.valueOf(numero + " - " + domanda.getTestoDomanda()));
					numero++;
					cellaTesto.addElement(testo);
					PdfPCell cellaOpzioni = new PdfPCell();
					cellaOpzioni.setBorder(PdfPCell.NO_BORDER);
					com.itextpdf.text.List unordered = new com.itextpdf.text.List(com.itextpdf.text.List.UNORDERED, com.itextpdf.text.List.ALPHABETICAL);
					for (String risposta : domanda.getOpzioniRisposta()){
						unordered.add(risposta);
					}
//					Paragraph answerP = new Paragraph("Risposta: ____");
//					answerP.setKeepTogether(true);
//					answerP.setSpacingBefore(2);
//					answerP.setAlignment(Element.ALIGN_RIGHT);
//					unordered.getLastItem().add(answerP); //lo aggiunge attaccato all'ultima risposta
					tabellaDomanda.addCell(cellaTesto);
					cellaOpzioni.addElement(unordered);
					tabellaDomanda.addCell(cellaOpzioni);
					orderedList.add(tabellaDomanda);
					totale += domanda.getPunteggioBase();
					Paragraph pti = new Paragraph(String.valueOf("/"+domanda.getPunteggioBase()));
					pti.setAlignment(Element.ALIGN_RIGHT);
					pti.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
					qNumberCell = new PdfPCell(pti);
					qNumberCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					ptiTable.addCell(qNumberCell);
				}
				for (Question domanda : open){
					PdfPTable tabellaDomanda = new PdfPTable(1);
					tabellaDomanda.setSpacingAfter(domanda.getSpaziBianchi()); //aggiunge n spazi bianchi dopo la domanda
					tabellaDomanda.setWidthPercentage(100);
					PdfPCell cellaTesto = new PdfPCell();
					cellaTesto.setBorder(PdfPCell.NO_BORDER);
					Paragraph testo = new Paragraph(String.valueOf(numero + " - " + domanda.getTestoDomanda()));
					numero++;
					cellaTesto.addElement(testo);
					tabellaDomanda.addCell(cellaTesto);
					if (domanda.getImmagineDomanda() == "" || domanda.getImmagineDomanda().isEmpty()){
						
					}else{
						Image imageQuestion = Image.getInstance(new URL(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/domande/"+domanda.getImmagineDomanda()));
						PdfPCell imageCell = new PdfPCell(imageQuestion, false);
						imageCell.setBorder(PdfPCell.NO_BORDER);
						tabellaDomanda.addCell(imageCell);
					}
					orderedList.add(tabellaDomanda);
					totale += domanda.getPunteggioBase();
					Paragraph pti = new Paragraph(String.valueOf("/"+domanda.getPunteggioBase()));
					pti.setAlignment(Element.ALIGN_RIGHT);
					pti.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
					qNumberCell = new PdfPCell(pti);
					qNumberCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					ptiTable.addCell(qNumberCell);
				}
				Paragraph tot = new Paragraph(String.valueOf("/"+totale));
				tot.setAlignment(Element.ALIGN_RIGHT);
				PdfPCell totCell = new PdfPCell(tot);
				totCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				ptiTable.addCell(totCell);
				uni.setAlignment(Element.ALIGN_CENTER);
				corsoLaurea.setAlignment(Element.ALIGN_CENTER);
				annoAccademico.setAlignment(Element.ALIGN_CENTER);
				corso.setAlignment(Element.ALIGN_CENTER);
				date.setAlignment(Element.ALIGN_CENTER);
				date.setSpacingAfter(15);
				ptiTable.setWidthPercentage(100);
				document.add(uni);
				document.add(corsoLaurea);
				document.add(annoAccademico);
				document.add(corso);
				document.add(date);
				document.add(ptiTable);
				for (PdfPTable tabella : orderedList){
					document.add(tabella);
				}
				document.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}

	public void creaFileRisposte(Exam esame) throws GenericException, DocumentException, MalformedURLException, IOException{
		Document document = new Document(PageSize.A4);
		String nameExam = FILE_LOCATION + "/"+esame.getFileName() + "_SOL.pdf";
		PdfWriter.getInstance(document, new FileOutputStream(nameExam));
		document.open();
		LineSeparator line = new LineSeparator();
		String university = is.getInformazioniEsami().get(0).getUni();
		String cDL = is.getInformazioniEsami().get(0).getcDL();
		String aA = is.getInformazioniEsami().get(0).getAnnoAccademico();
		String nomeCorso = is.getInformazioniEsami().get(0).getNomeCorso();
		String regoleEsame = is.getInformazioniEsami().get(0).getRegoleEsame();
		Paragraph uni = new Paragraph(university, FontFactory.getFont(FontFactory.HELVETICA_BOLD));
		Paragraph corsoLaurea = new Paragraph("LAUREA IN "+cDL.toUpperCase(), FontFactory.getFont(FontFactory.HELVETICA));
		Paragraph annoAccademico = new Paragraph("ANNO ACCADEMICO: "+aA, FontFactory.getFont(FontFactory.HELVETICA));
		Paragraph corso = new Paragraph(nomeCorso, FontFactory.getFont(FontFactory.HELVETICA_BOLD));
		Paragraph date = new Paragraph(esame.getData(), FontFactory.getFont(FontFactory.HELVETICA));
		Paragraph datiStudente = new Paragraph("Cognome: _______________ Nome: _______________ Matricola: _______________");
		int numDomande = esame.getDomande().size();
		List<Question> domande = esame.getDomande();
		Map<Integer, Integer> puntiDom = esame.getPuntiDomande();
		for (Question domanda : domande){
			domanda.setPunteggioBase(puntiDom.get(domanda.getNumero()));
		}
		List<Integer> numeriDomande = new ArrayList<Integer>();
		for (Question domanda : domande){
			numeriDomande.add(domanda.getNumero());
		}
		PdfPTable punti = new PdfPTable(numDomande + 1);
		List<Question> open = new ArrayList<Question>();
		List<Question> close = new ArrayList<Question>();
		for (Question domanda : domande){
			if(domanda.isAperta())
				open.add(domanda);
			else
				close.add(domanda);
		}
		PdfPCell qNumberCell;
		for(int i = 0; i < numDomande; i++){
			scriviNumeroDomanda(punti, i);
		}
		String totString = "TOT";
		Paragraph totParag = new Paragraph(totString, FontFactory.getFont(FontFactory.HELVETICA_BOLD));
		PdfPCell cell2 = new PdfPCell(totParag);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		punti.addCell(cell2);
		int totale = 0;
		int numero = 1;
		List<PdfPTable> orderedList = new ArrayList<PdfPTable>();
		for(Question domanda : close){
			PdfPTable tabellaDomanda = new PdfPTable(1);
			tabellaDomanda.setWidthPercentage(100);
			PdfPCell cellaTesto = new PdfPCell();
			cellaTesto.setBorder(PdfPCell.NO_BORDER);
			Paragraph testo = new Paragraph(String.valueOf(numero + " - " + domanda.getTestoDomanda()));
			numero++;
			cellaTesto.addElement(testo);
			Paragraph risposta = new Paragraph("Risposta: " + domanda.getRispostaDomanda());
			PdfPCell cellaRisposta = new PdfPCell();
			cellaRisposta.setBorder(PdfPCell.NO_BORDER);
			cellaRisposta.addElement(risposta);
			tabellaDomanda.addCell(cellaTesto);
			tabellaDomanda.addCell(cellaRisposta);
			orderedList.add(tabellaDomanda);
			totale += domanda.getPunteggioBase();
			Paragraph ptiValue = new Paragraph(String.valueOf("/"+domanda.getPunteggioBase()));
			ptiValue.setAlignment(Element.ALIGN_RIGHT);
			ptiValue.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
			qNumberCell = new PdfPCell(ptiValue);
			qNumberCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			punti.addCell(qNumberCell);
			Paragraph risp = new Paragraph("RISPOSTA: _____");
			risp.setAlignment(Element.ALIGN_RIGHT);
		}
		for(Question domanda : open){
			PdfPTable tabellaDomanda = new PdfPTable(1);
			tabellaDomanda.setWidthPercentage(100);
			PdfPCell cellaTesto = new PdfPCell();
			cellaTesto.setBorder(PdfPCell.NO_BORDER);
			Paragraph testo = new Paragraph(String.valueOf(numero + " - " + domanda.getTestoDomanda()));
			numero++;
			cellaTesto.addElement(testo);
			Paragraph risposta = new Paragraph("Risposta: " + domanda.getRispostaDomanda());
			PdfPCell cellaRisposta = new PdfPCell();
			cellaRisposta.setBorder(PdfPCell.NO_BORDER);
			cellaRisposta.addElement(risposta);
			tabellaDomanda.addCell(cellaTesto);
			if(domanda.getImmagineDomanda().isEmpty() || domanda.getImmagineDomanda() == ""){
				tabellaDomanda.addCell(cellaRisposta);
			}else{
				Image imageQuestion = Image.getInstance(new URL(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/domande/"+domanda.getImmagineDomanda()));
				PdfPCell imageCell = new PdfPCell(imageQuestion, false);
				imageCell.setBorder(PdfPCell.NO_BORDER);
//				tabellaDomanda.addCell(imageCell);
//				tabellaDomanda.addCell(cellaRisposta);
			}
			orderedList.add(tabellaDomanda);
			totale += domanda.getPunteggioBase();
			Paragraph pti = new Paragraph(String.valueOf("/"+domanda.getPunteggioBase()));
			pti.setAlignment(Element.ALIGN_RIGHT);
			pti.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
			qNumberCell = new PdfPCell(pti);
			qNumberCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			qNumberCell.setVerticalAlignment(Element.ALIGN_CENTER);
			punti.addCell(qNumberCell);
		}
		Paragraph tot = new Paragraph(String.valueOf("/"+totale));
		tot.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell totCell = new PdfPCell(tot);
		totCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		punti.addCell(totCell);
		Paragraph regole = new Paragraph("Regole esame: "+regoleEsame, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8));
		regole.setSpacingAfter(8);
		uni.setAlignment(Element.ALIGN_CENTER);
		corsoLaurea.setAlignment(Element.ALIGN_CENTER);
		annoAccademico.setAlignment(Element.ALIGN_CENTER);
		corso.setAlignment(Element.ALIGN_CENTER);
		date.setAlignment(Element.ALIGN_CENTER);
		date.setSpacingAfter(15);
		datiStudente.setSpacingAfter(5);
		datiStudente.setAlignment(Element.ALIGN_CENTER);
		punti.setWidthPercentage(100);
		document.add(uni);
		document.add(corsoLaurea);
		document.add(annoAccademico);
		document.add(corso);
		document.add(date);
		document.add(punti);
		document.add(regole);
		document.add(line);
		for(PdfPTable tabella : orderedList){
			document.add(tabella);
		}
		document.close();
	}

	private void scriviNumeroDomanda(PdfPTable ptiTable, int i) {
		PdfPCell qNumberCell;
		Paragraph p = new Paragraph(String.valueOf(i+1));
		p.setAlignment(Element.ALIGN_CENTER);
		p.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
		qNumberCell = new PdfPCell(p);
		qNumberCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ptiTable.addCell(qNumberCell);
	}
}