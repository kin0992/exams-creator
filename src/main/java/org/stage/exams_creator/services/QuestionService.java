package org.stage.exams_creator.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.mongodb.morphia.query.Query;
import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Category;
import org.stage.exams_creator.models.Question;

public class QuestionService extends BaseService {

	public QuestionService() {
	}

	public List<Question> getDomande() throws GenericException {
		Query<Question> q = getDatastore().createQuery(Question.class).filter("cancellato = ", false);
		List<Question> domande = q.asList();
		List<Question> lista = new ArrayList<Question>();
		for (Question domanda : domande){
			if (domanda.isCancellato() == false)
				lista.add(domanda);
		}
		if (lista.isEmpty())
			throw new GenericException("404");
		Collections.sort(lista, new Comparator<Question>() {
			public int compare(Question q1, Question q2){
				return Integer.valueOf(q1.getNumero()).compareTo(q2.getNumero());
			}
		});
		return lista;
	}

	public List<Question> getDomandePerCategoria(String categoria) throws GenericException {
		List<Question> domande = getDomande();
		List<Question> result = new ArrayList<Question>();
		for (Question domanda : domande) {
			List<Category> categorie = domanda.getCategorie();
			for(Category categ : categorie){
				if (categ.getNome().equalsIgnoreCase(categoria))
					result.add(domanda);
			}
		}
		Collections.sort(result, new Comparator<Question>() {
			public int compare(Question q1, Question q2){
				return Integer.valueOf(q1.getNumero()).compareTo(q2.getNumero());
			}
		});
		return result;
	}
	
	public List<Question> getDomandeAperte(List<Question> questions) throws GenericException{
		if(questions.isEmpty()){
			List<Question> domande = getDomande();
			List<Question> result = new ArrayList<Question>();
			for (Question domanda : domande){
				if (domanda.isAperta())
					result.add(domanda);
			}
			return result;
		}else{
			List<Question> result = new ArrayList<Question>();
			for(Question domanda : questions){
				if(domanda.isAperta())
					result.add(domanda);
			}
			return result;
		}
		
	}
	
	public List<Question> getDomandeChiuse(List<Question> questions) throws GenericException{
		if(questions.isEmpty()){
			List<Question> domande = getDomande();
			List<Question> result = new ArrayList<Question>();
			for (Question domanda : domande){
				if (domanda.isAperta() == false)
					result.add(domanda);
			}
			return result;
		}else{
			List<Question> result = new ArrayList<Question>();
			for (Question domanda : questions){
				if (domanda.isAperta() == false)
					result.add(domanda);
			}
			return result;
		}
	}
	
	public List<Question> getDomandePaginated(List<Question> domande, int start, int size) throws GenericException{
		int inizio, fine, dimension;
		if (start == 1)
			inizio = start - 1;
		else
			inizio = start;
		List<Question> sublista = new ArrayList<Question>();
		fine = inizio + size;
		if (domande.isEmpty()){
			throw new GenericException("404");
		}
		else{
			dimension = domande.size();
		}
		if (inizio > dimension) {
			throw new GenericException("400");
		}
		if (size > dimension || fine > dimension){
			fine = dimension;
			sublista =  domande.subList(inizio, fine);
		}
		else
			sublista =  domande.subList(inizio, fine);
		return sublista;
	}

	public Question getQuestion(int number) throws GenericException {
		Query<Question> query = getDatastore().createQuery(Question.class)
				.field("numero").equal(number);
		List<Question> question = query.asList();
		if (question.isEmpty())
			throw new GenericException("404");
		return question.get(0);
	}

	public Question addQuestion(String testo, List<String> risposta,
			List<Category> categorie, String immagine, List<String> opzioni,
			int punteggioBase, int spaziBianchi) throws GenericException {
		if(testo.isEmpty() || punteggioBase < 0 || punteggioBase > 10 || categorie.isEmpty())
			throw new GenericException("400");
		Question domanda = new Question(questionNumber(),testo.trim(), risposta, categorie, immagine, opzioni, punteggioBase, spaziBianchi);
		getDatastore().save(domanda);
		return domanda;
	}
	
	public Question updateQuestion(int idQuestion, String newTesto, List<String> newRisposta, List<Category> newCategorie, String newImmagine, List<String> newOpzioni, int newPunteggioBase, int spaziBianchi) throws GenericException{
		if(newPunteggioBase < 0 || newPunteggioBase > 10 || newCategorie.isEmpty())
			throw new GenericException("400");
		Question updatedDomanda = getQuestion(idQuestion);
		if(newTesto != updatedDomanda.getTestoDomanda())
			updatedDomanda.setTestoDomanda(newTesto);
		if(newRisposta != null) {
			updatedDomanda.setRispostaDomanda(newRisposta);
		}
		updatedDomanda.setCategorie(newCategorie);
		if(newImmagine == "" || newImmagine.isEmpty() || newImmagine == null)
			updatedDomanda.setImmagineDomanda(updatedDomanda.getImmagineDomanda());
		else
			updatedDomanda.setImmagineDomanda(newImmagine);
		if(spaziBianchi != 0)
			updatedDomanda.setSpaziBianchi(spaziBianchi);
		else
			updatedDomanda.setSpaziBianchi(0);
		updatedDomanda.setOpzioniRisposta(newOpzioni);
		if(newPunteggioBase != 0 || updatedDomanda.getPunteggioBase() != newPunteggioBase)
			updatedDomanda.setPunteggioBase(newPunteggioBase);
		getDatastore().save(updatedDomanda);
		return updatedDomanda;
	}
			

	public void removeQuestion(int number) throws GenericException {
		Query<Question> query = getDatastore().createQuery(Question.class).filter(
				"numero = ", number);
		Question domanda = query.asList().get(0);
		domanda.setCancellato(true);
		getDatastore().save(domanda);
		ExamService es = new ExamService();
		AnswerService as = new AnswerService();
		as.removeRisposteOfDeletedQuestion(number);
		es.removeEsamiOfDeletedQuestion(number);
		getDatastore().delete(domanda);
	}

	public void aumentaContatore(List<Question> domande) {
		for (Question domanda : domande) {
			int cont = domanda.getContatore();
			domanda.setContatore(cont + 1);
			getDatastore().save(domanda);
		}
	}

	public void decrementQuestionCont(List<Question> domande) throws GenericException{
		for (Question domanda : domande){
			int cont = domanda.getContatore();
			if(cont > 0){
				domanda.setContatore(cont - 1);
				getDatastore().save(domanda);
			}
		}
	}

	
	public void aggiungiAppello(List<Question> domande, String appello) {
		for (Question domanda : domande) {
			if(domanda.getAppelli().isEmpty() || domanda.getAppelli().size() == 0){
				List<String> appelli = new ArrayList<String>();
				appelli.add(appello);
				domanda.setAppelli(appelli);
			}
			else{
				List<String> appelli = domanda.getAppelli();
				appelli.add(appello);
			}
			getDatastore().save(domanda);
		}
	}
	
	public void rimuoviAppelli(List<Question> domande, String appello) {
		for (Question domanda : domande) {
			domanda.getAppelli().remove(appello);
			getDatastore().save(domanda);
		}
	}
	
	public void removeQuestionOfDeletedCategory(String nome) throws GenericException{
		List<Question> domande = getDatastore().createQuery(Question.class).asList();
		if (domande.isEmpty())
			return;
		for (Question domanda : domande) {
			List<Category> categorie = domanda.getCategorie();
			for (Category categoria : categorie){
				if(categoria.getNome().equalsIgnoreCase(nome))
					removeQuestion(domanda.getNumero());
			}
		}
	}
	public void orderQuestionByCont(List<Question> domande) {
		Collections.sort(domande, new Comparator<Question>(){
			public int compare(Question q1, Question q2){
				return Integer.valueOf(q1.getContatore()).compareTo(q2.getContatore());
			}
		});
	}
	
	public void orderQuestionByLastAppello(List<Question> domande) {
		List<String> lastAppelli = new ArrayList<String>();
		for(Question domanda : domande){
			int size = domanda.getAppelli().size();
			lastAppelli.add(domanda.getAppelli().get(size -1));
		}
		Collections.sort(domande, new Comparator<Question>(){
			public int compare(Question a1, Question a2){
				int a1S = a1.getAppelli().size();
				int a2S = a2.getAppelli().size();
				return Integer.valueOf(a1.getAppelli().get(a1S-1).compareTo(a2.getAppelli().get(a2S-1)));
			}

		});
		Collections.reverseOrder();
	}
	
	private int questionNumber() {
		List<Question> lista = getDatastore().createQuery(Question.class).asList();
		if (lista.isEmpty())
			return 1;
		List<Integer> numeri = new ArrayList<Integer>();
		for (Question domanda : lista) {
			numeri.add(domanda.getNumero());
		}
		int max = Collections.max(numeri);
		List<Integer> appoggio = new ArrayList<Integer>();
		for (int i = 1; i <= max; i++){
			appoggio.add(i);
		}
		appoggio.removeAll(numeri);
		if(appoggio.isEmpty())
			return max+1;
		Collections.sort(appoggio);
		return appoggio.get(0);
	}
}
