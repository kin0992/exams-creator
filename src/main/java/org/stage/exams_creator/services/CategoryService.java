package org.stage.exams_creator.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import org.mongodb.morphia.query.Query;
import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Category;

public class CategoryService  extends BaseService {
	
	public CategoryService() {
	}
	
	public List<Category> getCategorie() throws GenericException{
		Query<Category> query = getDatastore().createQuery(Category.class);
		List<Category> categorie = query.asList();
		if (categorie.isEmpty())
			throw new GenericException("404");
		Collections.sort(categorie, new Comparator<Category>() {
			public int compare(Category c1, Category c2){
				return c1.getNome().compareTo(c2.getNome());
			}
		});
		return categorie;
	}
	
	public List<Category> getCategoriePaginated(int start, int size) throws GenericException{
		int inizio, fine, dimension;
		if (start == 1)
			inizio = start - 1;
		else
			inizio = start;
		fine = inizio + size;
		List<Category> result = getCategorie();
		dimension = result.size();
		if (start > dimension)
			throw new GenericException("400");
		if (size > dimension || fine > dimension){
			fine = dimension;
			return result.subList(inizio, fine);
		}
		else
			return result.subList(inizio, fine);
	}
	
	public Category getCategoria(String nome) throws GenericException {
		Query<Category> query = getDatastore().createQuery(Category.class).field("nome").equal(nome.toLowerCase());
		List<Category> categoria = query.asList();
		if (categoria.isEmpty())
			throw new GenericException("404");
		return categoria.get(0);
	}
	
	//POST
	public Category addCategoria(String nome, String descrizione) throws GenericException {
		String nomeCategoria = nome.trim().replaceAll(" ", "_");
		if (nome.trim().isEmpty())
			throw new GenericException("400");
		Category categoria = new Category(categoryNumber(), nomeCategoria.toLowerCase(), descrizione.trim());
		boolean trovato = controllaEsistenza(nomeCategoria);
		if (!trovato) 
			getDatastore().save(categoria);
		else
			throw new GenericException("409");
		return categoria;
	}
	
	//PUT
	public Category updateCategoria(String nome, String newNome, String descrizione) throws GenericException{
		Category categoria = getCategoria(nome);
		String newDescrizione, defName;
		if (newNome.equalsIgnoreCase(nome))
			defName = categoria.getNome();
		else{
			if (controllaEsistenza(newNome))
				throw new GenericException("409");
			defName = newNome;
			defName = defName.trim().replaceAll(" ", "_");
		}
		if(descrizione == null)
			newDescrizione = categoria.getDescrizione();
		else 
			newDescrizione = descrizione;
		categoria.setNome(defName.trim());
		categoria.setDescrizione(newDescrizione.trim());
		getDatastore().save(categoria);
		return categoria;
	}
	
	//DELETE
	public void removeCategoria(String nome) throws GenericException {
		Query<Category> query = getDatastore().createQuery(Category.class)
				.filter("nome = ", nome);
		Category categoria = query.asList().get(0);
		categoria.setCancellato(true);
		getDatastore().save(categoria);
		QuestionService qs = new QuestionService();
		qs.removeQuestionOfDeletedCategory(nome);
		getDatastore().delete(query);
	}
	private boolean controllaEsistenza(String nome){
		boolean trovato = false;
		List<Category> categorie = getDatastore().createQuery(Category.class).asList();
		if (categorie.isEmpty())
			return trovato;
		for(Category categ : categorie) {
			if(categ.getNome().equalsIgnoreCase(nome)){
				trovato = true;
			}
		}
		return trovato;
	}
	private int categoryNumber(){
		List<Category> categorie = getDatastore().createQuery(Category.class).asList();
		if (categorie.isEmpty())
			return 1;
		List<Integer> numeri = new ArrayList<Integer>();
		for(Category categoria : categorie){
			numeri.add(categoria.getId());
		}
		int max = Collections.max(numeri);
		List<Integer> appoggio = new ArrayList<Integer>();
		for (int i = 1; i <= max; i++){
			appoggio.add(i);
		}
		appoggio.removeAll(numeri);
		if (appoggio.isEmpty())
			return max+1;
		Collections.sort(appoggio);
		return appoggio.get(0);
	}
}
