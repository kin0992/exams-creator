package org.stage.exams_creator.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mongodb.morphia.query.Query;
import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Info;

public class InfoService extends BaseService {

	public InfoService() {
	}

	public List<Info> getInformazioniEsami() throws GenericException{
		Query<Info> query = getDatastore().createQuery(Info.class);
		List<Info> informazioni = query.asList();
		if(informazioni.isEmpty())
			throw new GenericException("404");
		return informazioni;
		
	}
	
	public Info getInformazioniEsame(int id) throws GenericException{
		Query<Info> query = getDatastore().createQuery(Info.class).filter("id =", id);
		List<Info> informazioni = query.asList();
		if(informazioni.isEmpty())
			throw new GenericException("404");
		Info info = informazioni.get(0);
		return info;
	}

	public Info addInfo(String uni, String cDL, String aA, String nomeCorso, String regoleEsame){
		Info informazioni = new Info(infoNumber(), uni.trim(), cDL.toUpperCase().trim(), aA, nomeCorso.trim(), regoleEsame.trim());
		getDatastore().save(informazioni);
		return informazioni;
	}
	
	public Info updateInformazioniEsami(int id, String uni, String cDL, String aA, String nomeCorso, String regoleEsame) throws GenericException{
		Info info = getDatastore().createQuery(Info.class).asList().get(0);
		info.setId(id);
		info.setUni(uni);
		info.setcDL(cDL);
		info.setAnnoAccademico(aA);
		info.setNomeCorso(nomeCorso);
		info.setRegoleEsame(regoleEsame);
		getDatastore().save(info);
		return info;
	}
	
	public void deleteInfo(int id){
		Info info = getDatastore().createQuery(Info.class).filter("id =", id).asList().get(0);
		getDatastore().delete(info);
	}
	private int infoNumber() {
		List<Info> lista = getDatastore().createQuery(Info.class).asList();
		if (lista.isEmpty())
			return 1;
		List<Integer> numeri = new ArrayList<Integer>();
		for (Info info : lista) {
			numeri.add(info.getId());
		}
		int max = Collections.max(numeri);
		List<Integer> appoggio = new ArrayList<Integer>();
		for (int i = 1; i <= max; i++){
			appoggio.add(i);
		}
		appoggio.removeAll(numeri);
		if(appoggio.isEmpty())
			return max+1;
		Collections.sort(appoggio);
		return appoggio.get(0);
	}
}
