package org.stage.exams_creator.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.mongodb.morphia.query.Query;
import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Exam;
import org.stage.exams_creator.models.Question;


public class ExamService extends BaseService {
	
	PDFGeneratorService pdf = new PDFGeneratorService();
	QuestionService qs = new QuestionService();
	EditableFileService efs = new EditableFileService();

	public ExamService() {
	}

	public List<Exam> getEsami() throws GenericException {
		List<Exam> esami = getDatastore().createQuery(Exam.class).asList();
		if (esami.isEmpty())
			throw new GenericException("404");
		Collections.sort(esami, new Comparator<Exam>(){
			public int compare (Exam e1, Exam e2){
				return Integer.valueOf(e1.getId()).compareTo(Integer.valueOf(e2.getId()));
			}
		});
		return esami;
	}

	public Exam getEsame(String id) throws GenericException {
		if (id.length() != 6) {
			throw new GenericException("400");
		}
		List<Exam> esame = getDatastore().createQuery(Exam.class).filter("id =", id)
				.asList();
		if (esame.isEmpty()) {
			throw new GenericException("404");
		}
		return esame.get(0);
	}

	 public List<Exam> getEsamiPerAnno(int anno) throws GenericException {
		 List<Exam> esami = getEsami();
		 List<Exam> esamiPerAnno = new ArrayList<Exam>();
		 for (Exam esame : esami) {
			 if (esame.getId().substring(0, 4).equals(String.valueOf(anno)))
				 esamiPerAnno.add(esame);
		 }
		 return esamiPerAnno;
	 }
	 
	 public List<Exam> getEsamiPaginated(List<Exam> esami, int start, int size) throws GenericException{
		 int inizio, fine, dimension;
		 if (start == 1)
				inizio = start - 1;
			else
				inizio = start;
		 fine = inizio + size;
		 if (esami.isEmpty()){
			 throw new GenericException("404");
		 }
		 dimension = esami.size();
		 if (start > dimension)
				throw new GenericException("400");
		 if (size > dimension || fine > dimension){
			 fine = dimension;
			 return esami.subList(inizio, fine);
		 }
		 else
			 return esami.subList(inizio, fine);
	 }

	public Exam creaEsame(List<Question> domande, Map<Integer, Integer> domandePunti, int appello, String data, int logoUni, int logoDisco) throws GenericException, MalformedURLException, IOException {
		String date = data;
		data = data.replaceAll("-", "");
		String id = data.substring(0, 4);
		id += String.valueOf(0);
		id += String.valueOf(appello);
		for (Question domanda : domande){
			domanda.setPunteggioBase(domandePunti.get(domanda.getNumero()));
		}
		if (date.isEmpty() || String.valueOf(appello).isEmpty() || appello < 0
				|| appello > 7 || domande.isEmpty())
			throw new GenericException("400");
		String fileName = "SD"+id;
		Exam esame = new Exam(id, date, domande, appello, fileName, domandePunti, logoUni, logoDisco);
		boolean trovato = controllaEsistenza(id);
		if (!trovato){
			getDatastore().save(esame);
		}
		else
			throw new GenericException("409");
		return esame;
	}
	
	public Exam updateExam(String examId, List<Question> domande, int appello, String data, Map<Integer, Integer> puntiDomande, int logoUni, int logoDisco) throws GenericException, MalformedURLException, IOException{
		Exam newExam = new Exam();
		Exam oldExam = getEsame(examId);
		int newAppello;
		String newData;
		String newId;
		newExam.setCongelato(oldExam.isCongelato());
		newExam.setCreato(oldExam.isCreato());
		if(puntiDomande.size() == 0 || puntiDomande.isEmpty()){
			newExam.setPuntiDomande(oldExam.getPuntiDomande());
		}else{
			newExam.setPuntiDomande(puntiDomande);
		}
		if(data == null){
			newData = oldExam.getData();
		}
		else{
			newData = data;
		}
		if(appello == 0){
			newAppello = oldExam.getAppelloNumero();
			newId = examId;
		}
		else if(appello != oldExam.getAppelloNumero()){
			newAppello = appello;
			newId = examId.substring(0, 5);
			newId += String.valueOf(appello);
			if(controllaEsistenza(newId) == false)
				removeEsame(examId);
			else throw new GenericException("409");
		}else{
			newAppello = appello;
			newId = examId;
		}
		if (appello < 0	|| appello > 7)
			throw new GenericException("400");
		newExam.setId(newId);
		if(domande.isEmpty())
			newExam.setDomande(oldExam.getDomande());
		else
			{
			newExam.setDomande(domande);
			}
		if(oldExam.getAppelloNumero() != newAppello){
			newExam.setAppelloNumero(newAppello);
		}
		else
			newExam.setAppelloNumero(oldExam.getAppelloNumero());
		if(oldExam.getData().equals(newData) == false){
			newExam.setData(newData);
		}
		else
			newExam.setData(oldExam.getData());
		newExam.setLogoDisco(logoDisco);
		newExam.setLogoUni(logoUni);
		newExam.setFileName(oldExam.getFileName());
		getDatastore().save(newExam);
		return newExam;
	}
	
	public void removeEsame(String id) throws GenericException {
		Query<Exam> query = getDatastore().createQuery(Exam.class).filter("id = ",
				id);
		Exam esame = query.asList().get(0);
		List<Question> domande = esame.getDomande();
		esame.setCancellato(true);
		getDatastore().save(esame);
		if(esame.isCreato()){
			qs.decrementQuestionCont(domande);
			qs.rimuoviAppelli(domande, esame.getData());
		}
		AnswerService as = new AnswerService();
		as.removeRisposteOfDeletedExam(id);
		getDatastore().delete(query);
	}

	public void bloccaEsame(String examId) throws GenericException{
		Exam esame = getEsame(examId);
		esame.setCongelato(true);
		getDatastore().save(esame);
	}
	
	public void sbloccaEsame(String examId) throws GenericException{
		Exam esame = getEsame(examId);
		esame.setCongelato(false);
		getDatastore().save(esame);
	}
	
	public void creaPdf(String examId) throws Exception{
		Exam esame = getEsame(examId);
		esame.setCongelato(true);
		esame.setCreato(true);
		List<Question> domande = esame.getDomande();
		qs.aggiungiAppello(domande, esame.getData());
		qs.aumentaContatore(domande);
		getDatastore().save(esame);
		efs.createEditableFile(esame);
		pdf.creaFileEsame(esame);
		pdf.creaFileRisposte(esame);
	}
	
	public void aggiornaPresenti(String id, int presenti) throws GenericException {
		Exam esame = getEsame(id);
		esame.setStudentiPresenti(presenti);
		getDatastore().save(esame);
	}

	public void removeEsamiOfDeletedQuestion(int questionId) throws GenericException {
		Query<Exam> query = getDatastore().createQuery(Exam.class);
		List<Exam> esami = query.asList();
		if(esami.isEmpty())
			return;
		for(Exam esame : esami){
			List<Question> domande = new ArrayList<Question>();
			domande = esame.getDomande();
			for (Question domanda : domande){
				if (domanda.getNumero() == questionId){
					removeEsame(esame.getId());
					break;
				}
			}
		}
	}

	private boolean controllaEsistenza(String id) {
		boolean trovato = false;
		List<Exam> lista = getDatastore().createQuery(Exam.class).asList();
		if (lista.isEmpty())
			trovato = false;
		for (Exam esame : lista) {
			if (esame.getId().equalsIgnoreCase(id))
				trovato = true;
		}
		return trovato;
	}
}