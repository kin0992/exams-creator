package org.stage.exams_creator.services;

import java.net.URI;
import java.util.ArrayList;

import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.style.Border;
import org.odftoolkit.simple.style.StyleTypeDefinitions.CellBordersType;
import org.odftoolkit.simple.style.StyleTypeDefinitions.HorizontalAlignmentType;
import org.odftoolkit.simple.table.Cell;
import org.odftoolkit.simple.table.Table;
import org.stage.exams_creator.models.Exam;
import org.stage.exams_creator.models.Question;


public class EditableFileService {
	
	InfoService is = new InfoService();
	public static String FILE_LOCATION ="";
	public static String HOST_NAME="";
	public static String IMAGE_LOCATION ="";
	public static String SCHEME = "";
	public static int PORT;
	
	public EditableFileService() {
	}

	public void createEditableFile(Exam esame) throws Exception{
		TextDocument document;
		String university = is.getInformazioniEsami().get(0).getUni();
		String cDL = is.getInformazioniEsami().get(0).getcDL();
		String aA = is.getInformazioniEsami().get(0).getAnnoAccademico();
		String nomeCorso = is.getInformazioniEsami().get(0).getNomeCorso();
		String regoleEsame = is.getInformazioniEsami().get(0).getRegoleEsame();
		String  nomeCognome = "Cognome: _______________ Nome: _______________ Matricola: _______________";
		try{
			String destination = FILE_LOCATION + "/"+esame.getFileName() + ".odt";
			document = TextDocument.newTextDocument();
			int uniL, discoL;
			uniL = esame.getLogoUni();
			discoL = esame.getLogoDisco();
			System.out.println(uniL);
			System.out.println(discoL);
			if (uniL + discoL == 2){
				Table loghiTable = document.addTable(1, 2);
				loghiTable.getCellByPosition(0, 0).setImage(new URI(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo_bicocca.png"));
				loghiTable.getCellByPosition(1, 0).setImage(new URI(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo_disco.png"));
			}else{
				if(uniL > discoL){
					Table loghiTable = document.addTable(1, 1);
					loghiTable.getCellByPosition(0, 0).setImage(new URI(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo_bicocca.png"));
				}else if (discoL > uniL){
					Table loghiTable = document.addTable(1, 1);
					loghiTable.getCellByPosition(0, 0).setImage(new URI(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo_disco.png"));
				}
			}
//			document.addTable(1, 1).getCellByPosition(0, 0).setImage(new URI(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/webapp/logo_bicocca.png"));
			document.addParagraph(university).setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			document.addParagraph(cDL).setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			document.addParagraph(aA).setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			document.addParagraph(nomeCorso).setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			document.addParagraph(esame.getData()).setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			document.addParagraph(nomeCognome).setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			document.addParagraph(regoleEsame);
			java.util.List<Question> domande = esame.getDomande();
			java.util.List<Question> openQuestion = new ArrayList<Question>();
			java.util.List<Question> closeQuestion = new ArrayList<Question>();
			for(Question domanda: domande){
				if (domanda.isAperta())
					openQuestion.add(domanda);
				else
					closeQuestion.add(domanda);
			}
			closeQuestion.addAll(openQuestion);
			java.util.List<Question> orderedQuestion = closeQuestion;
			int numeroDomande = orderedQuestion.size();
			Table pointTable = document.addTable(2, numeroDomande + 1);
			for (int i = 0; i < numeroDomande; i++){
				Cell cellaNumeroDomanda = pointTable.getCellByPosition(i, 0);
				cellaNumeroDomanda.setStringValue(String.valueOf(i + 1));
				cellaNumeroDomanda.setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			}
			Cell cellaTot = pointTable.getCellByPosition(numeroDomande, 0);
			cellaTot.setStringValue("TOT");
			cellaTot.setHorizontalAlignment(HorizontalAlignmentType.CENTER);
			int index = 0;
			for (int i = 0; i < numeroDomande; i++){
				int qNum = i+1;
				Question actualQuestion = orderedQuestion.get(i);
				if(actualQuestion.isAperta() == false){
					Table questionTable = Table.newTable(document, 2, 1);
					Cell textCell = questionTable.getCellByPosition(0, 0);
					textCell.setBorders(CellBordersType.NONE, Border.NONE);
					textCell.setStringValue(qNum + " - " + actualQuestion.getTestoDomanda());
					Cell optionsCell = questionTable.getCellByPosition(0, 1);
					optionsCell.setBorders(CellBordersType.NONE, Border.NONE);
					for (int j = 0; j < actualQuestion.getOpzioniRisposta().size(); j++){
						int number = j+1;
						String opzione = actualQuestion.getOpzioniRisposta().get(j);
						optionsCell.addParagraph(number + " - " + opzione);
					}
				}else{
					if(actualQuestion.getImmagineDomanda() == "" || actualQuestion.getImmagineDomanda().isEmpty() || actualQuestion.getImmagineDomanda() == null){
						Table questionTable = Table.newTable(document, 2, 1);
						Cell textCell = questionTable.getCellByPosition(0, 0);
						textCell.setBorders(CellBordersType.NONE, Border.NONE);
						textCell.setStringValue(qNum + " - " + actualQuestion.getTestoDomanda());
						Cell answerCell = questionTable.getCellByPosition(0, 1);
						answerCell.setBorders(CellBordersType.NONE, Border.NONE);
						for (int j = 0; j < actualQuestion.getSpaziBianchi(); j++){
							answerCell.addParagraph("");
						}
					}else{
						Table questionTable = Table.newTable(document, 3, 1);
						Cell textCell = questionTable.getCellByPosition(0, 0);
						textCell.setBorders(CellBordersType.NONE, Border.NONE);
						textCell.setStringValue(qNum + " - " + actualQuestion.getTestoDomanda());
						Cell imageCell = questionTable.getCellByPosition(0, 1);
						imageCell.setBorders(CellBordersType.NONE, Border.NONE);
						imageCell.setImage(new URI(SCHEME + "://" + HOST_NAME + ":"+ PORT + "/" + "exams-creator/assets/images/domande/"+actualQuestion.getImmagineDomanda()));
						Cell answerCell = questionTable.getCellByPosition(0, 2);
						answerCell.setBorders(CellBordersType.NONE, Border.NONE);
						for (int j = 0; j < actualQuestion.getSpaziBianchi(); j++){
							answerCell.addParagraph("");
						}
					}
				}
				Cell cellaPunti = pointTable.getCellByPosition(index, 1);
				cellaPunti.setStringValue("/"+String.valueOf(actualQuestion.getPunteggioBase()));
				cellaPunti.setHorizontalAlignment(HorizontalAlignmentType.RIGHT);
				index++;
			}
			Cell cellaTotPunti = pointTable.getCellByPosition(numeroDomande, 1);
			int totale = 0;
			for (Question domanda : domande){
				totale += domanda.getPunteggioBase();
			}
			cellaTotPunti.setHorizontalAlignment(HorizontalAlignmentType.RIGHT);
			cellaTotPunti.setStringValue("/"+String.valueOf(totale));
			cellaTotPunti.setHorizontalAlignment(HorizontalAlignmentType.RIGHT);
			document.save(destination);
		}catch(Exception e){
			System.err.println("ERROR: unable to create output file.");
		}
	}
}