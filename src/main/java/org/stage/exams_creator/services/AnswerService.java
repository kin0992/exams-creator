package org.stage.exams_creator.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import org.mongodb.morphia.query.Query;
import org.stage.exams_creator.exceptions.GenericException;
import org.stage.exams_creator.models.Answer;
import org.stage.exams_creator.models.Exam;
import org.stage.exams_creator.models.Question;

public class AnswerService extends BaseService {

	public AnswerService() {
	}

	// GET
	public List<Answer> getRisposte(String examId) throws GenericException {
		Query<Answer> q = getDatastore().createQuery(Answer.class);
		List<Answer> risposte = q.asList();
		if(risposte.isEmpty())
			throw new GenericException("404");
		List<Answer> risposteEsame = new ArrayList<Answer>();
		for (Answer risposta : risposte) {
			if (risposta.getEsame().getId().equalsIgnoreCase(examId))
				risposteEsame.add(risposta);
		}
		if (risposteEsame.isEmpty())
			throw new GenericException("404");
		Collections.sort(risposteEsame, new Comparator<Answer>() {
			public int compare(Answer a1, Answer a2){
				return Integer.valueOf(a1.getId()).compareTo(a2.getId());
			}
		});
		return risposteEsame;
	}

	// GET risposte specifico esame
	public Answer getRisposta(String examId, int id) throws GenericException {
		List<Answer> risposte = getRisposte(examId);
		Answer risposta = new Answer();
		for (Answer risp : risposte) {
			if (risp.getId() == id)
				risposta = risp;
		}
		return risposta;
	}
	
	public List<Answer> getRispostaConRisposta(String examId, String risposta) throws GenericException {
		Query<Answer> query = getDatastore().createQuery(Answer.class).filter("rispostaData = ", risposta);
		List<Answer> risposte = query.asList();
		if(risposte.isEmpty())
			throw new GenericException("404");
		List<Answer> risposteEsame = new ArrayList<Answer>();
		for (Answer risp : risposte) {
			if (risp.getEsame().getId().equalsIgnoreCase(examId))
				risposteEsame.add(risp);
		}
		if(risposteEsame.isEmpty())
			throw new GenericException("404");
		return risposteEsame;
	}
	
	public List<Answer> getRispostePaginated(List<Answer> risposte, int start, int size) throws GenericException{
		int inizio, fine, dimension;
		if (start == 1)
			inizio = start - 1;
		else
			inizio = start;
		List<Answer> sublista = new ArrayList<Answer>();
		fine = inizio + size;
		if (risposte.isEmpty()){
			throw new GenericException("404");
		}
		else{
			dimension = risposte.size();
		}
		if (inizio > dimension) {
			throw new GenericException("400");
		}
		if (size > dimension || fine > dimension){
			fine = dimension;
			sublista =  risposte.subList(inizio, fine);
		}
		else
			sublista =  risposte.subList(inizio, fine);
		return sublista;
	}

	// POST
	public Answer creaRisposta(int occorrenza, double punteggio, double correttezza, String rispostaData, Exam esame,
			Question domanda) throws GenericException {
		if (correttezza > 1 || correttezza < 0 || occorrenza < 0)
			throw new GenericException("400");
		Answer risposta = new Answer();
		List<Answer> risposte = getDatastore().createQuery(Answer.class).filter("domanda = ", domanda).filter("esame = ", esame).filter("rispostaData = ", rispostaData).filter("correttezza = ", correttezza).asList();
		if (risposte.isEmpty()){
			risposta = new Answer(answerNumber(), occorrenza, punteggio, correttezza, rispostaData, esame, domanda);
		}else{
//			int oldOccorrenze = risposte.get(0).getOccorrenze();
			risposta = risposte.get(0);
			int newOccorrenza = occorrenza;
			risposta.setOccorrenze(newOccorrenza);
		}
		getDatastore().save(risposta);
		return risposta;
	}

	// PUT
	public Answer updateRisposta(String examId, int id, int occorrenza, double correttezza) throws GenericException {
		if (correttezza > 1 || correttezza < 0 || occorrenza < 0)
			throw new GenericException("400");
		Answer newRisposta = getRisposta(examId, id);
		newRisposta.setCorrettezza(correttezza);
		newRisposta.setPunteggio(correttezza * newRisposta.getDomanda().getPunteggioBase());
		newRisposta.setOccorrenze(occorrenza);
		getDatastore().save(newRisposta);
		return newRisposta;
	}

	// DELETE
	public void removeRisposta(int id) {
		Query<Answer> query = getDatastore().createQuery(Answer.class).filter(
				"id = ", id);
		Answer risposta = query.asList().get(0);
		risposta.setCancellato(true);
		getDatastore().delete(query);
	}
	
	protected void removeRisposteOfDeletedQuestion(int questionId){
			Query<Answer> query = getDatastore().createQuery(Answer.class);
			List<Answer> risposte = query.asList();
			if (risposte.isEmpty())
				return;
			for (Answer risposta : risposte){
				if(risposta.getDomanda().isCancellato())
					removeRisposta(risposta.getId());
			}
		}

	protected void removeRisposteOfDeletedExam(String examId){
		Query<Answer> query = getDatastore().createQuery(Answer.class);
		List<Answer> risposte = query.asList();
		for (Answer risposta : risposte){
			if(risposta.getEsame().isCancellato()){
				removeRisposta(risposta.getId());
			}
		}
	}
	
	private int answerNumber() {
		List<Answer> risposte = getDatastore().createQuery(Answer.class).asList();
		if (risposte.isEmpty())
			return 1;
		List<Integer> numeri = new ArrayList<Integer>();
		for (Answer risposta : risposte) {
			numeri.add(risposta.getId());
		}
		int max = Collections.max(numeri);
		List<Integer> appoggio = new ArrayList<Integer>();
		for (int i = 1; i <= max; i++) {
			appoggio.add(i);
		}
		appoggio.removeAll(numeri);
		if (appoggio.isEmpty())
			return max + 1;
		Collections.sort(appoggio);
		return appoggio.get(0);
	}
}
