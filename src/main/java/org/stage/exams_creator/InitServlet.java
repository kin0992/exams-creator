package org.stage.exams_creator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.stage.exams_creator.endpoints.QuestionResource;
import org.stage.exams_creator.services.EditableFileService;
import org.stage.exams_creator.services.PDFGeneratorService;

public class InitServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		super.init();
		String imagePath = getServletContext().getRealPath("/assets/images/domande");
		QuestionResource.IMAGE_LOCATION = imagePath;
		EditableFileService.IMAGE_LOCATION = imagePath;
		String hostName = getServletContext().getContextPath();
		EditableFileService.HOST_NAME = hostName;
		PDFGeneratorService.HOST_NAME = hostName;
		String filesLocation = getServletContext().getRealPath("/files");
		PDFGeneratorService.FILE_LOCATION = filesLocation;
		EditableFileService.FILE_LOCATION = filesLocation;
	}
}
